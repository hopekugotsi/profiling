/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


const Category = Vue.component('category', {
         
        data: function () {
            return {
                categories: [],
                category: {
                    catId: '',
                    category: ''
                },
                control: {
                    edit: false,
                    save: false
                },
                search: ''

            }
        },
        props: ['update']
        ,
        mounted(){
           this.searchCategory(); 
        },
        computed: {
            control() {
                //console.log("props 1" + this.update);
                return {
                    edit: this.update,
                    save: !this.update};
            }

        },
        methods: {
            searchCategory(search) {
                //console.log(search);
                axios.get("/fetchcategory", {
                    params: {
                        search: search
                    }
                }).then(function (response) {
                    this.categories = response.data;
                }.bind(this));
            },
            setCategory(category) {
                this.category.catId = category.catId;
                this.category.category = category.name;
                this.search = category.name;
                // document.getElementById("myDropdown3").classList.toggle("show");
                console.log(category.name);

            },
            save() {
                axios.post("/savecategory", {
                    name: this.category.category,
                    // comment : this.comment
                }).then(function (response) {
                    if (response.data === "saved") {
                        swal({
                            icon: "success",
                            text: "Thanks for your input."
                        });
                    } else {
                        swal({
                            icon: "error",
                            text: "something went wrong."
                        });
                    }
                }.bind(this))
                this.clear()
            },
            update() {
                axios.post("/updatecategory", {
                    catId: this.category.catId,
                    name: this.category.category,
                    // comment : this.comment
                }).then(function (response) {
                    if (response.data === "saved") {
                        swal({
                            icon: "success",
                            text: "Category updated successfuly."
                        });
                    } else {
                        swal({
                            icon: "success",
                            text: "something went wrong."
                        });
                    }
                }.bind(this))
                this.clear()
            },
            clear(){
                this.category.category = '';
                this.category.catId = '';
                this.search = '';
            }
        },
        template:'#category'
    });

