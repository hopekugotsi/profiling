/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

const Discipline = Vue.component('discipline', {
	template: '#discipline',
	data: function () {
		return {
			categories: [],
			disciplines: [],
			category: {
				catId: '',
				category: ''
			},
			discipline: {
				disciplineId: '',
				discipline: ''
			},
			control: {
				edit: false,
				save: false
			},
			discipline1: '',
			search: ''

		}
	},
	props: ['update']
	,
	mounted() {
		this.searchCategory();
	},
	computed: {
		control() {
			return {
				edit: this.update,
				save: !this.update
			};
		}

	},
	methods: {
		searchCategory(search) {
			//console.log(search);
			axios.get("/fetchcategory", {
				params: {
					search: search
				}
			}).then(function (response) {
				this.categories = response.data;
			}.bind(this));
		},
		setCategory(category) {
			this.category.catId = category.catId;
			this.category.category = category.name;
			//this.search = category.name;
			// document.getElementById("myDropdown3").classList.toggle("show");
			console.log(category.name);

		},
		searchDiscipline(search) {
			console.log(search);
			axios.get("/searchdisciplines", {
				params: {
					search: search
				}
			}).then(function (response) {
				this.disciplines = response.data;
			}.bind(this));
		},
		setDiscipline(discipline) {
			this.category.catId = discipline.catid;
			this.category.category = discipline.name;
			this.discipline.disciplineId = discipline.disciplineid;
			this.discipline1 = discipline.discipline;
			this.search = discipline.discipline;
			console.log(" catId >> " + discipline.catId);
			// document.getElementById("myDropdown3").classList.toggle("show");

		},
		saveDiscipline() {
			const fd = new FormData();
			fd.append('catId1', this.category.catId);
			fd.append('discipline', this.discipline1);
			//fd.append('studentId', this.studentId);
			console.log(this.discipline1 + " discipline " + this.category.catId + "category");
			axios.post("/savediscipline", fd, {
				onUploadProgress: uploadEvent => {
					console.log('Upload Progress: ' + Math.round(uploadEvent.loaded / uploadEvent.total * 100 + '%'))
				}
			}, {
				// body : this.input.body,
				//title : this.input.title
			}).then(function (response) {
				if (response.data === "saved") {
					swal({
						icon: "success",
						text: "Thanks for your input."
					});
				}
				else {
					swal({
						icon: "success",
						text: "something went wrong."
					});
				}
			}.bind(this))
			this.clear()
		},
		updateDiscipline() {
			const fd = new FormData();
			fd.append('catId', this.category.catId);
			fd.append('discipline', this.discipline1);
			fd.append('disId', this.discipline.disciplineId);
			//fd.append('studentId', this.studentId);
			console.log(this.discipline1 + " discipline " + this.category.catId + "category");
			axios.post("/updatediscipline", fd, {
				onUploadProgress: uploadEvent => {
					console.log('Upload Progress: ' + Math.round(uploadEvent.loaded / uploadEvent.total * 100 + '%'))
				}
			}, {
				// body : this.input.body,
				//title : this.input.title
			}).then(function (response) {
				if (response.data === "saved") {
					swal({
						icon: "success",
						text: "Category updated successfuly."
					});
				}
				else {
					swal({
						icon: "success",
						text: "something went wrong."
					});
				}
			}.bind(this));
			this.clear();
		},
		clear(){
			this.category.category = '';
			this.category.catId = '';
			this.search = '';
			this.discipline.discipline = '';
			this.discipline.disciplineId = '';
			this.discipline1 = '';
		}
	}
});



