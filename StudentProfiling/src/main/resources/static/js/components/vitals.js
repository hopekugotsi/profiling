const Vitals = Vue.component('app-admin1', {

    data: function () {
        return {
            studentId: '',
            studentFName: '',
            studentMName: '',
            studentSurname: '',
            sstudent: '',
            students: [],
            svital:'',
            vitalId: ' ',
            bmi: '',
            height: '',
            weight: '',
            vitals: [],
            dateCol: ''


        }
    },
	props:{
		update:{
			type: Boolean
		}
	},
	computed:{
		control(){
			return {edit: this.update,
				save: !this.update};
		}
		
	},
    mounted() {
        //this.fetchMedals();
    },
    methods: {

        saveVital() {
            const fd = new FormData();
            
            fd.append('adate1', this.dateCol);
            fd.append('studentId', this.studentId);
            fd.append('vitalId', this.vitalId);
            fd.append('bmi', this.bmi);
            fd.append('weight', this.weight);
            fd.append('height', this.height);
            console.log(this.vitalId);
            //console.log(this.studentId);
            //console.log(this.medalId);
            axios.post("/savevitalsinfor", fd, {
                onUploadProgress: uploadEvent => {
                    console.log('Upload Progress: ' + Math.round(uploadEvent.loaded / uploadEvent.total * 100 + '%'))
                }
            }, {
                // body : this.input.body,
                //title : this.input.title
            }).then(function (response) {
                if (response.data === "saved") {
                    swal({
                        icon: "success",
                        text: "saved"
                    });
                } else {
                    swal({
                        icon: "success",
                        text: "Error occured consult system administrator."
                    });
                }
            }.bind(this))
            this.clear()
        },
        searchStudents(search) {
            console.log("running");
            axios.get("/searchstudents", {
                params: {
                    search: search
                }
            }).then(function (response) {
                this.students = response.data;
            }.bind(this));
        },
        setStudent(studentId, name, mname, surname) {
            this.studentId = studentId;
            this.sstudent = name;
            this.studentFName = name;
            this.studentMName = mname;
            this.studentSurname = surname;
            //document.getElementById("myDropdown2").classList.toggle("show");
            console.log(studentId);

        }
        ,
        searchVital(search) {
            console.log("running");
            axios.get("/searchvitalbystdname", {
                params: {
                    search: search
                }
            }).then(function (response) {
                this.vitals = response.data;
            }.bind(this));
        },
        setVital(vital) {
            this.studentId = vital.studentid;
            this.sstudent = vital.name;
            this.studentFName = vital.firstname;
            this.studentMName = vital.middlename;
            this.studentSurname = vital.surname;
            this.vitalId = vital.vitalid;
            this.bmi = vital.bmi;
            this.weight = vital.weight;
            this.height = vital.height;
            //document.getElementById("myDropdown2").classList.toggle("show");
            console.log(studentId);

        },
        clear(){
            this.studentId = '';
            this.description = '';
            this.studentFName = '';
            this.studentMName = '';
            this.studentSurname = '';
            this.sstudent = '';
            this.students = '';
            this.medalId = '';
            this.medal = '';
            this.dateawarded = '';
        }
    },
    template: '#vitals'
})
