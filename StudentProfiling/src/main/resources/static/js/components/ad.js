const AD = Vue.component('app-admin1', {

    data: function () {
        return {
            studentId: '',
            studentFName: '',
            studentMName: '',
            studentSurname: '',
            sstudent: '',
            students: [],
            santidope:'',
            dopeId: ' ',
            heartrate: '',
            dope: '',
            ads: [],
            dateCol: ''


        }
    },
	props:{
		update:{
			type: Boolean
		}
	},
	computed:{
		control(){
			return {edit: this.update,
				save: !this.update};
		}
		
	},
    mounted() {
        //this.fetchMedals();
    },
    methods: {

        saveAD() {
            const fd = new FormData();
            
            fd.append('adate1', this.dateCol);
            fd.append('studentId', this.studentId);
            fd.append('dopeId', this.dopeId);
            fd.append('heartrate', this.heartrate);
            fd.append('dope', this.dope);
            console.log(this.dopeId);
            //console.log(this.studentId);
            //console.log(this.medalId);
            axios.post("/saveantidopeinfor", fd, {
                onUploadProgress: uploadEvent => {
                    console.log('Upload Progress: ' + Math.round(uploadEvent.loaded / uploadEvent.total * 100 + '%'))
                }
            }, {
                // body : this.input.body,
                //title : this.input.title
            }).then(function (response) {
                if (response.data === "saved") {
                    swal({
                        icon: "success",
                        text: "saved"
                    });
                } else {
                    swal({
                        icon: "success",
                        text: "Error occured consult system administrator."
                    });
                }
            }.bind(this))
            this.clear()
        },
        searchStudents(search) {
            console.log("running");
            axios.get("/searchstudents", {
                params: {
                    search: search
                }
            }).then(function (response) {
                this.students = response.data;
            }.bind(this));
        },
        setStudent(studentId, name, mname, surname) {
            this.studentId = studentId;
            this.sstudent = name;
            this.studentFName = name;
            this.studentMName = mname;
            this.studentSurname = surname;
            //document.getElementById("myDropdown2").classList.toggle("show");
            console.log(studentId);

        }
        ,
        searchAD(search) {
            console.log("running");
            axios.get("/searchadbystdname", {
                params: {
                    search: search
                }
            }).then(function (response) {
                this.ads = response.data;
            }.bind(this));
        },
        setAD(dope) {
            this.studentId = dope.studentid;
            this.santidope = dope.firstname;
            this.sstudent = dope.firstname;
            this.studentFName = dope.firstname;
            this.studentMName = dope.middlename;
            this.studentSurname = dope.surname;
            this.dopeId = dope.antidopeid;
            this.heartrate = dope.heartrate;
            this.dope = dope.dope;
            this.dateCol = new Date(dope.datecollected).toDateString("yyyy-mm-dd");
            
            //document.getElementById("myDropdown2").classList.toggle("show");
            console.log(this.dateCol);

        },
        clear(){
            this.studentId = '';
            this.description = '';
            this.studentFName = '';
            this.studentMName = '';
            this.studentSurname = '';
            this.sstudent = '';
            this.dopeId = '';
            this.heartrate = '';
            this.dateCol = '';
            this.ads = '';
        }
    },
    template: '#antidope'
})
