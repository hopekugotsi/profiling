/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

const Records = Vue.component('records', {
	template:'#records',
	data: function(){
		return {
			records:[],
			events: [],
			
			event:{
				
				eventId:'',
				event:''
			},
			record :{
				recordId:'',
                wRecord:'',
                rRecord:'',
                nRecord: ''
			},
			control: {
				edit: false,
				save:false
			},
			search: ''
			
		}
	},
	props:{
		update:{
			type: Boolean
		}
	},
	computed:{
		control(){
			return {edit: this.update,
				save: !this.update};
		}
		
	},
	methods: {
		searchRecords(search) {
	    	   console.log(search);
	           axios.get("/getrecordbyeventname", {
	        	   params:{
	        		   event: search
	        	   }
	           }).then(function (response) {
				 this.records = response.data;
				 console.log(this.records);
	          }.bind(this));
	      },
	      setRecord(record){
				 this.record.wBest = record.worldbest;
				 this.record.rBest = record.regionalbest;
				 this.record.nBest = record.nationalbest;
				 this.record.recordId = record.recordid;
				 this.record.event = record.event;
				 this.search = record.event;
				 this.record.eventId = record.eventid;
            	// document.getElementById("myDropdown3").classList.toggle("show");
            	 console.log(catId);  	 
		 },
		 
		 searchEvent(search) {
			console.log(search);
			axios.get("/searchevents", {
				params:{
					search: search
				}
			}).then(function (response) {
			  this.events = response.data;
		   }.bind(this));
	   },
	   setEvent(event){
			  
			  this.event.eventId = event.eventid;
			  this.event.event = event.discipline + " " + event.event;

			 // document.getElementById("myDropdown3").classList.toggle("show");
			  
	  },
         saveRecord(){
			const fd = new FormData();
			fd.append('event', this.event.eventId);
			fd.append('worldBest', this.record.wRecord);
			fd.append('regBest', this.record.rRecord);
			fd.append('natBest', this.record.nRecord);
			//console.log(this.event.event + " event " + this.unit.unitId + " unit "  + this.discipline.disciplineId + " discipline ");
			axios.post("/saverecord", fd, {
				onUploadProgress: uploadEvent => {
					console.log('Upload Progress: ' + Math.round(uploadEvent.loaded / uploadEvent.total * 100 + '%'))
				}
			}, {
				// body : this.input.body,
				//title : this.input.title
			}).then(function(response){
             	if(response.data === "saved"){
             		swal({
               		   icon: "success",
               		   text:"Thanks for your input."
               		 });
             	}
             	else{
             		swal({
               		   icon: "error",
               		   text:"something went wrong."
               		 });
             	}
             }.bind(this))
             this.clear()
		   },
		   updateRecord(){
			const fd = new FormData();
			fd.append('event', this.event.eventId);
			fd.append('worldBest', this.record.wRecord);
			fd.append('regionalBest', this.record.rRecord);
			fd.append('natBest', this.record.nRecord);
			fd.append('recordId', this.record.recordId);
			//console.log(this.event.event + " event " + this.unit.unitId + " unit "  + this.discipline.disciplineId + " discipline ");
			axios.post("/updaterecord", fd, {
				onUploadProgress: uploadEvent => {
					console.log('Upload Progress: ' + Math.round(uploadEvent.loaded / uploadEvent.total * 100 + '%'))
				}
			}, {
				// body : this.input.body,
				//title : this.input.title
			}).then(function(response){
             	if(response.data === "saved"){
             		swal({
               		   icon: "success",
               		   text:"Category updated successfuly."
               		 });
             	}
             	else{
             		swal({
               		   icon: "success",
               		   text:"something went wrong."
               		 });
             	}
             }.bind(this))
             this.clear()
           },
		   clear(){
			   this.record.wRecord = '';
			   this.record.nRecord = '';
			   this.record.rRecord = '';
			   this.search = '';
			   this.event.event = '';
			   this.event.eventId = '';
			   
		   }
	}
})
