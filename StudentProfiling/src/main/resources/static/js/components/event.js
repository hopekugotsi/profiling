/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


const Event = Vue.component('event', {
	template: '#event',
	data: function () {
		return {
			units: [],
			disciplines: [],
			events: [],
			category: {
				catId: '',
				category: ''
			},
			discipline: {
				disciplineId: '',
				discipline: ''
			},
			event: {
				eventId: '',
				event: ''
			},
			unit: {
				unit: '',
				unitId: ''
			},
			control: {
				edit: false,
				save: false
			},
			sevent: ''

		}
	},
	props: {
		update: {
			type: Boolean
		}
	},
	mounted() {
		this.searchUnits();
	},
	computed: {
		control() {
			return {
				edit: this.update,
				save: !this.update
			};
		}

	},
	methods: {
		searchUnits() {

			axios.get("/searchunits", {
			}).then(function (response) {
				this.units = response.data;
			}.bind(this));
		},
		setUnit(unit) {
			this.unit.unitId = unit.unitid;
			this.unit.unit = unit.unit;

		},
		searchDiscipline(search) {
			console.log(search);
			axios.get("/searchdisciplines", {
				params: {
					search: search
				}
			}).then(function (response) {
				this.disciplines = response.data;
			}.bind(this));
		},
		setDiscipline(discipline) {
			this.discipline.disciplineId = discipline.disciplineid;
			this.discipline.discipline = discipline.discipline;
			// document.getElementById("myDropdown3").classList.toggle("show");

		},
		searchEvent(search) {
			console.log(search);
			axios.get("/searchevents", {
				params: {
					search: search
				}
			}).then(function (response) {
				this.events = response.data;
			}.bind(this));
		},
		setEvent(event) {

			this.discipline.disciplineId = event.disciplineid;
			this.discipline.discipline = event.discipline;

			this.event.eventId = event.eventid;
			this.event.event = event.event;
			this.search = event.event;
			this.unit.unitId = event.unitid;
			this.unit.unit = event.unit;

			// document.getElementById("myDropdown3").classList.toggle("show");

		},
		saveEvent() {
			const fd = new FormData();
			fd.append('event', this.event.event);
			fd.append('unitId', this.unit.unitId);
			fd.append('disciplineId', this.discipline.disciplineId);
			//fd.append('studentId', this.studentId);
			console.log(this.event.event + " event " + this.unit.unitId + " unit "  + this.discipline.disciplineId + " discipline ");
			axios.post("/saveevent", fd, {
				onUploadProgress: uploadEvent => {
					console.log('Upload Progress: ' + Math.round(uploadEvent.loaded / uploadEvent.total * 100 + '%'))
				}
			}, {
				// body : this.input.body,
				//title : this.input.title
			}).then(function (response) {
				if (response.data === "saved") {
					swal({
						icon: "success",
						text: "Thanks for your input."
					});
				}
				else {
					swal({
						icon: "success",
						text: "something went wrong."
					});
				}
			}.bind(this))
			this.clear()
		},
		updateEvent() {
			const fd = new FormData();
			fd.append('eventId', this.event.eventId);
			fd.append('event', this.event.event);
			fd.append('unitId', this.unit.unitId);
			fd.append('disciplineId', this.discipline.disciplineId);
			//fd.append('studentId', this.studentId);
			console.log(this.discipline1 + " discipline " + this.category.catId + "category");
			axios.post("/updateevent", fd, {
				onUploadProgress: uploadEvent => {
					console.log('Upload Progress: ' + Math.round(uploadEvent.loaded / uploadEvent.total * 100 + '%'))
				}
			}, {
				// body : this.input.body,
				//title : this.input.title
			}).then(function (response) {
				if (response.data === "saved") {
					swal({
						icon: "success",
						text: "Category updated successfuly."
					});
				}
				else {
					swal({
						icon: "success",
						text: "something went wrong."
					});
				}
			}.bind(this))
			this.clear()
		},
		clear(){
			this.sevent = '';
			this.discipline.discipline = '';
			this.discipline.disciplineId = '';
			this.unit.unit = '';
			this.unit.unitId = '';
			this.event.event = '';
			this.event.eventId = '';
		}
	}
});

