package com.developer.sp.statscontrollers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import com.developer.sp.models.SoccerStatistics;
import com.developer.sp.services.SoccerService;

@RestController
public class Soccer {
	
	@Autowired
	private SoccerService soccerService;
	
	@PostMapping(value = "savesoccerstat")
	public String registerSchool(@ModelAttribute SoccerStatistics soccer){
		return soccerService.saveStat(soccer);
		
	}

}
