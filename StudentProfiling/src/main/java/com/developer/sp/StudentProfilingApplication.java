package com.developer.sp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import com.developer.sp.config.FileStorageProperties;

@SpringBootApplication
@EnableConfigurationProperties({FileStorageProperties.class})
public class StudentProfilingApplication {

	public static void main(String[] args) {
		SpringApplication.run(StudentProfilingApplication.class, args);
	}

}
