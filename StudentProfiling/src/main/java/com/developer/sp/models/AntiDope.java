package com.developer.sp.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Data;

@Entity
@Table(name = "antidope")
@Data
public class AntiDope {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "antidopeid")
	private long dopeId;
	
	@ManyToOne
	@JoinColumn(name = "studentid")
	private Student student;
	
	@Column(name = "heartrate")
	private double heartRate;
	
	@Column(name = "dope")
	private String dope;
	
	@Column(name = "datecollected")
	private Date dateCol;
        
        @ManyToOne
	@JoinColumn(name = "email")
	private Admin creator;

	
}
