package com.developer.sp.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "province")

public class Province {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "provinceid")
	private int provinceId;
	
	@Column(name = "province")
	private String province;

	public int getProvinceId() {
		return provinceId;
	}

	public void setProvinceId(int provinceId) {
		this.provinceId = provinceId;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

}
