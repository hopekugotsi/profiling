package com.developer.sp.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "discipline")

public class Discipline {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "disciplineid")
	private long disciplineId;
	
	@Column(name = "discipline")
	private String discipline;
	
	@ManyToOne
	@JoinColumn(name = "catid")
	private TF_Cat category;

	public long getDisciplineId() {
		return disciplineId;
	}

	public void setDisciplineId(long disciplineId) {
		this.disciplineId = disciplineId;
	}

	public String getDiscipline() {
		return discipline;
	}

	public void setDiscipline(String discpline) {
		this.discipline = discpline;
	}

	public TF_Cat getCategory() {
		return category;
	}

	public void setCategory(TF_Cat category) {
		this.category = category;
	}
	
	
}
