package com.developer.sp.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "city")

public class City {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "cityid")
	private long cityId;
	
	@Column(name = "city")
	private String city;
	
	@ManyToOne
	@JoinColumn(name = "provinceid")
	private Province province;

	public long getCityId() {
		return cityId;
	}

	public void setCityId(long cityId) {
		this.cityId = cityId;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public Province getProvince() {
		return province;
	}

	public void setProvince(Province province) {
		this.province = province;
	}
	
	

}
