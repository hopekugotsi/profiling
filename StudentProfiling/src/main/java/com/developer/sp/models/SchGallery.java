package com.developer.sp.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "schgallery")
public class SchGallery {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "galleryid")
	private long galleryId;
	
	@Column(name = "link")
	private String link;
	
	@Column(name = "description")
	private String description;
	
	@ManyToOne
	@JoinColumn(name = "email")
	private Admin creator;
	
	@ManyToOne
	@JoinColumn(name = "schoolid")
	private School school;
	
	@Column(name = "dateUploaded")
	private Date dateUploaded;

	public long getGalleryId() {
		return galleryId;
	}

	public void setGalleryId(long galleryId) {
		this.galleryId = galleryId;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Admin getCreator() {
		return creator;
	}

	public void setCreator(Admin creator) {
		this.creator = creator;
	}

	public School getSchool() {
		return school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public Date getDateUploaded() {
		return dateUploaded;
	}

	public void setDateUploaded(Date dateUploaded) {
		this.dateUploaded = dateUploaded;
	}
	
	

}
