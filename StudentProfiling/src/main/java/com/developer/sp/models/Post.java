package com.developer.sp.models;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "posts")
public class Post {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	private String title;
	private String body;
	private String featuredImage;
	private Date creationDate;
	
	@ManyToOne
	@JoinColumn(name = "email")
	private Admin creator;
	
	public Admin getCreator() {
		return creator;
	}
	public void setCreator(Admin admin) {
		this.creator = admin;
	}
	public void setId(long id) {
		this.id = id;
	}
	public Post() {
		
	}
	public long getId() {
		return id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
	
	
	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	public String getFeaturedImage() {
		return featuredImage;
	}
	public void setFeaturedImage(String featuredImage) {
		this.featuredImage = featuredImage;
	}
	@Override
	public String toString() {
		return "Post [id=" + id + ", title=" + title + ", body=" + body + " , featureImage=" + featuredImage + ", dateCreated=" + creationDate + ", creator="
				+ creator + "]";
	}

}
