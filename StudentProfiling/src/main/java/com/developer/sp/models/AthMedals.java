package com.developer.sp.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import java.util.Date;

@Entity
@Table(name = "athmedals")
public class AthMedals {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "athmedalid")
	private long athMedalId;
	
	@Column(name = "description")
	private String description;
	
	@Column(name = "awarddate")
	private Date adate;
	
	@ManyToOne
	@JoinColumn(name = "medalid")
	private Medals medal;
	
	@ManyToOne
	@JoinColumn(name = "studentid")
	private Student student;
	
	@ManyToOne
	@JoinColumn(name = "email")
	private Admin creator;
	
	

	public Admin getCreator() {
		return creator;
	}

	public void setCreator(Admin creator) {
		this.creator = creator;
	}

	
	public long getAthMedalId() {
		return athMedalId;
	}

	public void setAthMedalId(long athMedalId) {
		this.athMedalId = athMedalId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getAdate() {
		return adate;
	}

	public void setAdate(Date adate) {
		this.adate = adate;
	}

	public Medals getMedal() {
		return medal;
	}

	public void setMedal(Medals medal) {
		this.medal = medal;
	}

	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}
	
	

}
