package com.developer.sp.models;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;

@Entity
@Data
@Table(name = "tf_cat")
public class TF_Cat {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "catid")
	private long catId;
	
	@Column(name = "name")
	private String name;

	@Column(name = "registereddate")
	private Date regDate;

	
	
	
}
