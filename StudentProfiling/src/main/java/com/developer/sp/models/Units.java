package com.developer.sp.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;

@Entity
@Table(name = "units")
@Data
public class Units {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "unitid")
	private long unitId;
	
	@Column(name = "unit")
	private String unit;
        
        @Column(name = "siunit")
	private String siUnit;

}
