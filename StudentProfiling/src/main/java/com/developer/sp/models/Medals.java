package com.developer.sp.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "medals")

public class Medals {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "medalid")
	private long medalId;
	
	@Column(name = "name")
	private String name;

	public long getMedalId() {
		return medalId;
	}

	public void setMedalId(long medalId) {
		this.medalId = medalId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	
}
