package com.developer.sp.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;


@Entity
@Table(name = "schools")
public class School {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "schoolid")
	private long schoolId;
	
	@Column(name = "name")
	private String name;
	
	@Column(name = "contact")
	private String contact;
	
	@Column(name = "username")
	private String username;
	
	@Column(name = "moto")
	private String moto;
	
	@Column(name = "datecreated")
	private Date dateCreated;
	
	@Column(name = "est")
	private Date est;
	
	@Column(name = "profileimg")
	private String profileImg;
	
	@ManyToOne
	@JoinColumn(name = "city")
	private City city;
	
	//private double distanceFromCBD;
	
	private String address;
	
	@ManyToOne
	@JoinColumn(name = "email")
	private Admin creator;
	
	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	
	public long getSchoolId() {
		return schoolId;
	}

	public void setSchoolId(long schoolId) {
		this.schoolId = schoolId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

		

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
/*
	public String getSchoolHead() {
		return schoolHead;
	}

	public void setSchoolHead(String schoolHead) {
		this.schoolHead = schoolHead;
	}

	public String getContact() {
		return contact;
	}
*/
	public void setContact(String contact) {
		this.contact = contact;
	}
/*
	public double getDistanceFromCBD() {
		return distanceFromCBD;
	}

	public void setDistanceFromCBD(double distanceFromCBD) {
		this.distanceFromCBD = distanceFromCBD;
	}
*/
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Admin getCreator() {
		return creator;
	}

	public void setCreator(Admin creator) {
		this.creator = creator;
	}
	public String getProfileImg() {
		return profileImg;
	}

	public void setProfileImg(String profileImg) {
		this.profileImg = profileImg;
	}
	public City getCity() {
		return city;
	}

	public void setCity(City city) {
		this.city = city;
	}

	public String getContact() {
		return contact;
	}

	public Date getEst() {
		return est;
	}

	public void setEst(Date est) {
		this.est = est;
	}

	public String getMoto() {
		return moto;
	}

	public void setMoto(String moto) {
		this.moto = moto;
	}
	

}
