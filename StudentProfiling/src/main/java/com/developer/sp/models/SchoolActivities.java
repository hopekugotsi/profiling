package com.developer.sp.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "schoolactivities")
public class SchoolActivities {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "schActivityid")
	private long schActivityId;
	
	@ManyToOne
	@JoinColumn(name = "activityid")
	private Activities activity;
	
	@ManyToOne
	@JoinColumn(name = "schoolid")
	private School school;

	public long getSchActivityId() {
		return schActivityId;
	}

	public void setSchActivityId(long schActivityId) {
		this.schActivityId = schActivityId;
	}

	public Activities getActivity() {
		return activity;
	}

	public void setActivity(Activities activity) {
		this.activity = activity;
	}

	public School getSchool() {
		return school;
	}

	public void setSchool(School school) {
		this.school = school;
	}
}
