package com.developer.sp.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "trackfield")
public class TrackField {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "trackid")
	private long trackId;
	
	@ManyToOne
	@JoinColumn(name = "eventid")
	private Event event;
	
	@Column(name = "personalbest")
	private double personalBest;
	
	@Column(name = "datecollected")
	private Date dateCollected;
	
	@ManyToOne
	@JoinColumn(name = "studentid")
	private Student student;

	public long getTrackId() {
		return trackId;
	}

	public void setTrackId(long trackId) {
		this.trackId = trackId;
	}

	public double getPersonalBest() {
		return personalBest;
	}

	public void setPersonalBest(long personalBest) {
		this.personalBest = personalBest;
	}

	public Date getDateCollected() {
		return dateCollected;
	}

	public void setDateCollected(Date dateCollected) {
		this.dateCollected = dateCollected;
	}

	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	public Event getEvent() {
		return event;
	}

	public void setEvent(Event event) {
		this.event = event;
	}

	public void setPersonalBest(double personalBest) {
		this.personalBest = personalBest;
	}
	
	
	
	
}
