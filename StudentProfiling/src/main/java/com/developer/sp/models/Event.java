package com.developer.sp.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "event")
public class Event {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "eventid")
	private long eventId;
	
	@Column(name = "event")
	private String event;

	
	@ManyToOne
	@JoinColumn(name = "unitid")
	private Units unit;

	
	@ManyToOne
	@JoinColumn(name = "disciplineid")
	private Discipline discipline;


	public long getEventId() {
		return eventId;
	}


	public void setEventId(long eventId) {
		this.eventId = eventId;
	}


	public String getEvent() {
		return event;
	}


	public void setEvent(String event) {
		this.event = event;
	}



	public Units getUnit() {
		return unit;
	}


	public void setUnit(Units unit) {
		this.unit = unit;
	}


	public Discipline getDiscipline() {
		return discipline;
	}


	public void setDiscipline(Discipline discipline) {
		this.discipline = discipline;
	}
	
	

}
