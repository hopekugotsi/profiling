package com.developer.sp.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "shop")

public class Shop {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "shopid")
	private long shopId;
	
	@Column(name = "shop")
	private String shop;
	
	@Column(name = "description")
	private double description;
	
	
	@Column(name = "url")
	private double url;
	
	
	@ManyToOne
	@JoinColumn(name = "disciplineid")
	private Admin admin;


}
