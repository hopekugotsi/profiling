package com.developer.sp.models;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "soccer")
public class SoccerStatistics {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long soccerStatId;
	
	private int pushPassingPower;
	private int drivePassingAcc;
	private int pushPassingAcc;
	private int drivePassingPower;
	private int shotOnGoalBallMot;
	private int shotOnGoalBallStat;
	private int ballControlArial;
	private int ballControlGrounded;
	private int ballControlJuggling;
	private int penaltyTaken;
	private int penaltyScored;
	private int againstGoalKeeper;
	private int againstDefAndGKeeper;
	private int interDef;
	private int wallPositioning;
	private int groundedball;
	private int chestHighballs;
	private int bouncingBalls;
	private int ballTipping;
	private int wallPositioning1;
	private int footwork;
	private int divingLeft;
	private int divingRight;
	private int longThrow;
	private int distribution;
	
	private Date creationDate;
	
	@ManyToOne
	@JoinColumn(name = "email")
	private Admin creator;
	
	@ManyToOne
	@JoinColumn(name = "studentID")
	private Student student;

	public long getSoccerStatId() {
		return soccerStatId;
	}

	public void setSoccerStatId(long soccerStatId) {
		this.soccerStatId = soccerStatId;
	}

	public int getPushPassingPower() {
		return pushPassingPower;
	}

	public void setPushPassingPower(int pushPassingPower) {
		this.pushPassingPower = pushPassingPower;
	}

	public int getDrivePassingAcc() {
		return drivePassingAcc;
	}

	public void setDrivePassingAcc(int drivePassingAcc) {
		this.drivePassingAcc = drivePassingAcc;
	}

	public int getShotOnGoalBallMot() {
		return shotOnGoalBallMot;
	}

	public void setShotOnGoalBallMot(int shotOnGoalBallMot) {
		this.shotOnGoalBallMot = shotOnGoalBallMot;
	}

	public int getShotOnGoalBallStat() {
		return shotOnGoalBallStat;
	}

	public void setShotOnGoalBallStat(int shotOnGoalBallStat) {
		this.shotOnGoalBallStat = shotOnGoalBallStat;
	}

	public int getBallControlArial() {
		return ballControlArial;
	}

	public void setBallControlArial(int ballControlArial) {
		this.ballControlArial = ballControlArial;
	}

	public int getBallControlGrounded() {
		return ballControlGrounded;
	}

	public void setBallControlGrounded(int ballControlGrounded) {
		this.ballControlGrounded = ballControlGrounded;
	}

	public int getBallControlJuggling() {
		return ballControlJuggling;
	}

	public void setBallControlJuggling(int ballControlJuggling) {
		this.ballControlJuggling = ballControlJuggling;
	}

	public int getPenaltyTaken() {
		return penaltyTaken;
	}

	public void setPenaltyTaken(int penaltyTaken) {
		this.penaltyTaken = penaltyTaken;
	}

	public int getPenaltyScored() {
		return penaltyScored;
	}

	public void setPenaltyScored(int penaltyScored) {
		this.penaltyScored = penaltyScored;
	}

	public int getAgainstGoalKeeper() {
		return againstGoalKeeper;
	}

	public void setAgainstGoalKeeper(int againstGoalKeeper) {
		this.againstGoalKeeper = againstGoalKeeper;
	}

	public int getAgainstDefAndGKeeper() {
		return againstDefAndGKeeper;
	}

	public void setAgainstDefAndGKeeper(int againstDefAndGKeeper) {
		this.againstDefAndGKeeper = againstDefAndGKeeper;
	}

	public int getInterDef() {
		return interDef;
	}

	public void setInterDef(int interDef) {
		this.interDef = interDef;
	}

	public int getWallPositioning() {
		return wallPositioning;
	}

	public void setWallPositioning(int wallPositioning) {
		this.wallPositioning = wallPositioning;
	}

	public int getGroundedball() {
		return groundedball;
	}

	public void setGroundedball(int groundedball) {
		this.groundedball = groundedball;
	}

	public int getChestHighballs() {
		return chestHighballs;
	}

	public void setChestHighballs(int chestHighballs) {
		this.chestHighballs = chestHighballs;
	}

	public int getBouncingBalls() {
		return bouncingBalls;
	}

	public void setBouncingBalls(int bouncingBalls) {
		this.bouncingBalls = bouncingBalls;
	}

	public int getBallTipping() {
		return ballTipping;
	}

	public void setBallTipping(int ballTipping) {
		this.ballTipping = ballTipping;
	}

	public int getWallPositioning1() {
		return wallPositioning1;
	}

	public void setWallPositioning1(int wallPositioning1) {
		this.wallPositioning1 = wallPositioning1;
	}

	public int getFootwork() {
		return footwork;
	}

	public void setFootwork(int footwork) {
		this.footwork = footwork;
	}

	public int getDivingLeft() {
		return divingLeft;
	}

	public void setDivingLeft(int divingLeft) {
		this.divingLeft = divingLeft;
	}

	public int getDivingRight() {
		return divingRight;
	}

	public void setDivingRight(int divingRight) {
		this.divingRight = divingRight;
	}

	public int getLongThrow() {
		return longThrow;
	}

	public void setLongThrow(int longThrow) {
		this.longThrow = longThrow;
	}

	public int getDistribution() {
		return distribution;
	}

	public void setDistribution(int distribution) {
		this.distribution = distribution;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Admin getCreator() {
		return creator;
	}

	public void setCreator(Admin creator) {
		this.creator = creator;
	}

	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	public int getPushPassingAcc() {
		return pushPassingAcc;
	}

	public void setPushPassingAcc(int pushPassingAcc) {
		this.pushPassingAcc = pushPassingAcc;
	}

	public int getDrivePassingPower() {
		return drivePassingPower;
	}

	public void setDrivePassingPower(int drivePassingPower) {
		this.drivePassingPower = drivePassingPower;
	}
	
	
	
}
