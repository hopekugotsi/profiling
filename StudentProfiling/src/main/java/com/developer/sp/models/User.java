package com.developer.sp.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import lombok.Data;

import org.hibernate.validator.constraints.Length;


@Entity
@Table(name = "users")
@Data
public class User {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	@NotNull
	@Email
	@Column(name = "email")
	private String email;
	
	@NotNull
	@Column(name = "firstname")
	private String firstName;

	@NotNull
	@Column(name = "lastname")
	private String lastName;

	@NotNull
	@Length(min = 5)
	private String password;
	
	
	private boolean active;
	
	
	private String roles;
	


}
