package com.developer.sp.models;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Data;

@Entity
@Table(name = "vitals")
@Data
public class Vitals {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "vitalid")
	private long vitalId;
	
	@ManyToOne
	@JoinColumn(name = "studentid")
	private Student student;
	
	@Column(name = "weight")
	private double weight;
	
	@Column(name = "height")
	private double height;
	
	@Column(name = "bmi")
	private double bmi;
	
	@Column(name = "datecollected")
	private Date dateCol;
        
        @ManyToOne
	@JoinColumn(name = "email")
	private Admin creator;

}
