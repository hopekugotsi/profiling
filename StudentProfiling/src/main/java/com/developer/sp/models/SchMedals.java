package com.developer.sp.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "schmedals")

public class SchMedals {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "schMedalid")
	private long schMedalId;
	
	@Column(name = "description")
	private String description;
	
	@Column(name = "awarddate")
	private Date adate;
	
	@ManyToOne
	@JoinColumn(name = "medalid")
	private Medals medal;
	
	@ManyToOne
	@JoinColumn(name = "schoolid")
	private School school;
	
	@ManyToOne
	@JoinColumn(name = "email")
	private Admin creator;

	
	public Admin getCreator() {
		return creator;
	}

	public void setCreator(Admin creator) {
		this.creator = creator;
	}

	
	public long getSchMedalId() {
		return schMedalId;
	}

	public void setSchMedalId(long schMedalId) {
		this.schMedalId = schMedalId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getAdate() {
		return adate;
	}

	public void setAdate(Date adate) {
		this.adate = adate;
	}

	public Medals getMedal() {
		return medal;
	}

	public void setMedal(Medals medal) {
		this.medal = medal;
	}

	public School getSchool() {
		return school;
	}

	public void setSchool(School school) {
		this.school = school;
	}


	
}
