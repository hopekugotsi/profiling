package com.developer.sp.models;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Data;

@Entity
@Data
@Table(name = "records")
public class Records {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "recordid")
	private long recordId;
	
	@Column(name = "isrecent")
	private Boolean isRecent;

	@Column(name = "dateset")
	private Date dateSet;
	
	@Column(name = "worldbest")
	private double worldBest;
	
	
	@Column(name = "nationalbest")
	private double natBest;
	
	@Column(name = "regionalbest")
	private double regBest;
	
	@ManyToOne
	@JoinColumn(name = "event")
	private Event event;

}
