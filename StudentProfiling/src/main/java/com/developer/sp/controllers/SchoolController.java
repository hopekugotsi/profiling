package com.developer.sp.controllers;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import com.developer.sp.models.School;
import com.developer.sp.services.SchoolService;
import org.springframework.web.bind.annotation.CrossOrigin;


@RestController
@CrossOrigin(origins = "*")
public class SchoolController {
	
	@Autowired
	private SchoolService schoolService;
	
	@PostMapping(value = "/admin/registerschool", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public String registerSchool(@ModelAttribute School school,@RequestParam long city ,@RequestParam String est1, @RequestParam("image") MultipartFile file) throws IOException, ParseException{
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		//school = new School();
		Date parsed = format.parse(est1);
		school.setEst(parsed);
		System.out.println("saving  " + school + " " + city);
		return schoolService.saveSchool(school,city, file);
		
	}
	
	@GetMapping(value = "getschool1")
	public School getSchool(@RequestParam String email) {
		return schoolService.getSchool(email);
	}
	
	@GetMapping(value = "getschool/{id}")
	public Map<String, Object> getSchool1(@PathVariable Long id) {
		//System.out.println(schoolService.getSchools());
		String query = " SELECT sch.NAME, sch.MOTO, hm.FIRSTNAME as HDFNAME, hm.MIDDLENAME as HDMNAME, hm.SURNAME as HDSURNAME, dr.FIRSTNAME as DRFNAME, dr.MIDDLENAME as DRMNAME, dr.SURNAME as DRSURNAME, sch.EST, ct.CITY FROM "
				+ "schools AS sch JOIN city AS ct ON sch.CITY = ct.CITYID JOIN "
				+ " coach AS hm ON sch.SCHOOL_ID = hm.SCHOOL_ID "
				
				+ "WHERE sch.SCHOOLID = '" + id + "' OR hm.POSTID = '" + 1 + "' OR dr.POSTID = '" + 2 + "'" ;
				
				/*"SELECT * FROM schools WHERE "
				+ "school_id LIKE '%" + id + "%'";*/
		/*
		System.out.println("return : " + schoolService.search(" SELECT sch.NAME, sch.PROFILEIMG,sch.MOTO, sch.EST, ct.CITY, pv.PROVINCE , hm.MIDDLENAME as HDMNAME, hm.SURNAME as HDSURNAME FROM "
				+ "SCHOOLS AS sch JOIN CITY AS ct ON sch.CITY = ct.CITYID JOIN PROVINCE as pv ON ct.PROVINCEID = "
				+ " pv.PROVINCEID  JOIN  COACH AS hm ON sch.SCHOOL_ID = hm.SCHOOL_ID WHERE "
				+ "sch.school_id = '" + id + "' AND hm.POSTID = '" + 1 + "'")); */
		return schoolService.search(" SELECT sch.schoolid, sch.name, sch.profileimg, sch.moto, sch.est, ct.city, pv.province , hm.MIDDLENAME as hdmname, hm.SURNAME as hdsurname FROM "
				+ "schools AS sch JOIN city AS ct ON sch.CITY = ct.CITYID JOIN province as pv ON ct.PROVINCEID = "
				+ " pv.PROVINCEID  JOIN  coach AS hm ON sch.SCHOOLID = hm.SCHOOLID WHERE "
				+ "sch.schoolid = '" + id + "' AND hm.POSTID = '" + 1 + "'").get(0);
	}
	
	@GetMapping(value = "getschools")
	public List<School> getSchools(@RequestParam String email) {
		
		return schoolService.getSchools();
	}
	/*
	@GetMapping(value = "getschoolactivities/{id}")
	public List<Map<String, Object>> getSchoolActivities(@PathVariable Long id) {
		System.out.println("response 12");
		String query = "SELECT * FROM activities as act JOIN  "
				+ "schoolactivities as schact ON act.activityid = schact.activityid"
				+ " WHERE schact.schoolid =  '" + id + "'";
		System.out.println("response " + schoolService.search(query));
		return schoolService.search(query);
	}
	*/
	@GetMapping(value = "getschoolactivitystats/")
	public List<Map<String, Object>> getSchoolActivityStats(@RequestParam Long id, @RequestParam Long activityName) {
		System.out.println("response 12");
		String query = "SELECT * FROM " + activityName + 
				 " WHERE schoolid =  '" + id + "' "
				 + " GROUP BY CREATIONDATE " +
				 		 "ORDER BY CREATIONDATE ASC";
		System.out.println("response " + schoolService.search(query));
		return schoolService.search(query);
	}
	
	@GetMapping(value = "searchschools")
	public List<Map<String, Object>> search(@RequestParam String search) {
		/*
		String query = "SELECT * FROM schools WHERE "
				+ "name LIKE '%" + search + "%'";*/
		//query = "SELECT * FROM schools";
		String query = "SELECT sch.schoolid, sch.name, sch.est, prv.province, ct.city  FROM schools as sch JOIN  "
				+ "city as ct ON sch.CITY = ct.CITYID JOIN "
				+ " province as prv ON ct.PROVINCEID = prv.PROVINCEID "
				+ " WHERE  sch.NAME LIKE  '%" + search + "%'";
		
		System.out.println("response " + schoolService.search(query));
		return schoolService.search(query);
	}
	
	@GetMapping(value = "searchschoolshome")
	public List<Map<String, Object>> search1(@RequestParam String name, @RequestParam String province, @RequestParam String city) {
		/*
		String query = "SELECT * FROM schools WHERE "
				+ "name LIKE '%" + search + "%'";*/
		//query = "SELECT * FROM schools";
		String query = "SELECT sch.schoolid, sch.name, sch.est, prv.province, ct.city, hm.firstname as hdfname, hm.middlename as hdmname, hm.surname as hdsurname  FROM schools as sch JOIN  "
				+ "city as ct ON sch.CITY = ct.CITYID JOIN "
				+ " province as prv ON ct.PROVINCEID = prv.PROVINCEID "
                                + "JOIN  coach AS hm ON sch.SCHOOLID = hm.SCHOOLID"
				+ " WHERE  sch.NAME LIKE  '%" + name + "%' AND ct.CITY LIKE '%" + city + "%' AND prv.PROVINCE LIKE '%" + province + "%' AND hm.POSTID = '" + 1 + "'";
		
		System.out.println("response " + query);
		return schoolService.search(query);
	}

}
