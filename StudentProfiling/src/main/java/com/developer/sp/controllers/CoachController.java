package com.developer.sp.controllers;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.developer.sp.config.MyUserDetails;
import com.developer.sp.dao.DataAccess;
import com.developer.sp.models.Coach;
import com.developer.sp.models.Student;
import com.developer.sp.repositories.CityRepo;
import com.developer.sp.repositories.CoachRepository;
import com.developer.sp.repositories.DesignationRepo;
import com.developer.sp.repositories.SchoolRepo;
import com.developer.sp.services.FileStorageService;
import com.developer.sp.services.StudentService;
import com.developer.sp.services.UserService;
import com.developer.sp.util.AppConstants;
import org.springframework.web.bind.annotation.CrossOrigin;

@RestController
@CrossOrigin(origins = "*")
public class CoachController {

	@Autowired
	private CoachRepository coachRepo;
	
	@Autowired
	private FileStorageService fileStorageService;
	
	@Autowired
	UserService userService;
	
	@Autowired
	SchoolRepo schoolRepo;
	
	@Autowired
	private DesignationRepo dRepo;

	@Autowired
	private DataAccess dataAccess;

	@Autowired
	private CityRepo cityRepo;
	
	@PostMapping(value = "registercoach", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public String registerSchool(@ModelAttribute Coach coach,@RequestParam Long schoolId, @RequestParam Long city, @RequestParam String dob1, @RequestParam Long postId, @RequestParam("image") MultipartFile file) throws IOException, ParseException{
		MyUserDetails userDetails =  (MyUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		
                //System.out.println("school " + schoolId);System.out.println("city " + city); System.out.println("post " + postId);
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		
		Date parsed = format.parse(dob1);
		coach.setDob(parsed);
		coach.setJoinDate(new Date());
		
		String fileName = fileStorageService.storeFile(file);
		String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath().path(AppConstants.DOWNLOAD_PATH)
				.path(fileName).toUriString();
		
		coach.setProfileImg(fileDownloadUri);
		coach.setCreator(userService.getUser(userDetails.getUsername()));
		coach.setSchool(schoolRepo.findById(schoolId).get());
		coach.setCity(cityRepo.getOne(city));
		coach.setDesignation(dRepo.findById(postId).get());
		
		System.out.println("saved");
		coachRepo.save(coach);
		return "saved";
		
	}
	
	@GetMapping(value = "gethmasterinfor/{schoolId}")
	public Map<String, Object> getHMaster(@PathVariable Long schoolId) {
		String query = "SELECT * FROM coach WHERE "
				+ "schoolid = '" + schoolId + "' AND postid = '" + 1 + "'";
		System.out.println("hmaster : " + dataAccess.run(query));
		return dataAccess.run(query).get(0);
	}
	
	@GetMapping(value = "getsdirectorinfor/{schoolId}")
	public Map<String, Object> getSdir(@PathVariable Long schoolId) {
		String query = "SELECT * FROM coach WHERE "
				+ "schoolid = '" + schoolId + "' AND postid = '" + 2 + "'";
		return dataAccess.run(query).get(0);
	}
	
	@GetMapping(value = "getasdirectorinfor/{schoolId}")
	public Map<String, Object> getAsdir(@PathVariable Long schoolId) {
		String query = "SELECT * FROM coach WHERE "
				+ "schoolid = '" + schoolId + "' AND postid = '" + 3 + "'";
		return dataAccess.run(query).get(0);
	}
	@GetMapping(value = "getcoachinfor/{schoolId}")
	public List<Map<String, Object>> getCoach(@PathVariable Long schoolId) {
		String query = "SELECT * FROM coach WHERE "
				+ "schoolid = '" + schoolId + "' AND postid = '" + 4 + "'";
		return dataAccess.run(query);
	}
	
}
