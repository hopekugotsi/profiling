package com.developer.sp.controllers;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.developer.sp.config.MyUserDetails;
import com.developer.sp.dao.DataAccess;
import com.developer.sp.models.AthGallery;
import com.developer.sp.models.AthMedals;
import com.developer.sp.repositories.AthMedalsRepo;
import com.developer.sp.repositories.MedalRepo;
import com.developer.sp.repositories.StudentRepo;
import com.developer.sp.services.UserService;
import com.developer.sp.util.AppConstants;
import java.util.ArrayList;
import java.util.HashMap;
import org.springframework.web.bind.annotation.CrossOrigin;

@RestController
@CrossOrigin(origins = "*")
public class AthMController {
	
	@Autowired
	UserService userService;
	
	@Autowired
	AthMedalsRepo amRepo;
	
	@Autowired
	MedalRepo medalRepo;
	
	@Autowired
	StudentRepo studentRepo;
	
	@Autowired
	DataAccess dataAccess;
	
	@PostMapping(value = "addathmedal", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public String registerSchool(@RequestParam Long studentId, @RequestParam Long medalId, @RequestParam String description, @RequestParam String adate1) throws ParseException{
		AthMedals medals = new AthMedals();
		
		MyUserDetails userDetails =  (MyUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		
		Date parsed = format.parse(adate1);
		
		medals.setAdate(parsed);
		
		
		
		medals.setCreator(userService.getUser(userDetails.getUsername()));
		medals.setStudent(studentRepo.findById(studentId).get());
		
		medals.setMedal(medalRepo.findById(medalId).get());
		
		medals.setDescription(description);
		
		amRepo.save(medals);
		return "saved";
		
	}
	
	@GetMapping(value = "getathgold/{studentId}")
	public List<Map<String, Object>> getGold(@PathVariable Long studentId) {
		String query = "SELECT * FROM athmedals WHERE "
				+ "studentid = '" + studentId + "' AND medalid = '" + 1 + "'";
		return dataAccess.run(query);
	}
	
	@GetMapping(value = "getathsilver/{studentId}")
	public List<Map<String, Object>> getSilver(@PathVariable Long studentId) {
		String query = "SELECT * FROM athmedals WHERE "
				+ "studentid = '" + studentId + "' AND medalid = '" + 2 + "'";
		return dataAccess.run(query);
	}
	
	@GetMapping(value = "getathbronze/{studentId}")
	public List<Map<String, Object>> getBronze(@PathVariable Long studentId) {
		String query = "SELECT * FROM athmedals WHERE "
				+ "studentid = '" + studentId + "' AND medalid = '" + 3 + "'";
		return dataAccess.run(query);
	}
        
        @GetMapping(value = "getathmedals/{studentId}")
	public List<Map<String, Object>> getMedals(@PathVariable Long studentId) {
		
                List<Map<String, List<Map<String, Object>>>> val = new ArrayList();
                Map<String, List<Map<String, Object>>> value;
                
                String query = "SELECT description, awarddate, medalid FROM athmedals WHERE "
				+ "studentid = '" + studentId + "'";
                
               // value.put("silver", dataAccess.run(silver));
               // value.put("bronze", dataAccess.run(bronze));
               // System.out.println("medals" + value);
                /*
                for(int i = 1; i < 4; i++){
                    String query = "SELECT * FROM athmedals WHERE "
				+ "studentid = '" + studentId + "' AND medalid = '" + i + "'";
                    value = new HashMap();
                    switch (i) {
                        case 1:
                            value.put("gold", dataAccess.run(query));
                            System.out.println("medals1 " + value);
                            break;
                        case 2:
                            value.put("silver", dataAccess.run(query));
                            System.out.println("medals2 " + value);
                            break;
                        default: 
                            value.put("bronze", dataAccess.run(query));
                            System.out.println("medals3 " + value);
                            break;
                    }
                    val.add(value);
                };
*/
                System.out.println("medals " + dataAccess.run(query));
		return dataAccess.run(query);
	}


}
