package com.developer.sp.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.developer.sp.dao.DataAccess;
import com.developer.sp.models.Designation;
import com.developer.sp.models.Province;
import com.developer.sp.repositories.DesignationRepo;
import com.developer.sp.repositories.ProvinceRepo;

@RestController
public class PostController {

	@Autowired
	private DesignationRepo dRepo;
	
	@Autowired
	private DataAccess dataAccess;
	
	@GetMapping(value = "fetchpost")
	public List<Designation> getPost() {
		System.out.println(dRepo.findAll());
		return dRepo.findAll();
	}
}
