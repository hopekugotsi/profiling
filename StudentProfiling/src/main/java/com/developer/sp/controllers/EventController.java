package com.developer.sp.controllers;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.developer.sp.dao.DataAccess;
import com.developer.sp.models.Discipline;
import com.developer.sp.models.Event;
import com.developer.sp.repositories.DisciplineRepository;
import com.developer.sp.repositories.EventRepo;
import com.developer.sp.repositories.UnitRepo;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;

@RestController
public class EventController {

    @Autowired
    private DataAccess dataAccess;
    
    @Autowired
    private DisciplineRepository disRepo;
    
    @Autowired
    private EventRepo eveRepo;
    
    @Autowired
    private UnitRepo unitRepo;

    @GetMapping(value = "searchevent")
    public List<Map<String, Object>> searchCity(@RequestParam String event, @RequestParam int discipline) {
        String query = "SELECT * FROM event WHERE "
                + "event LIKE '%" + event + "%' AND "
                + " disciplineid = '" + discipline + "'";
        System.out.println(dataAccess.run(query));
        //query = "SELECT * FROM schools";
        //System.out.println("response " + schoolService.search(query));
        return dataAccess.run(query);
    }

    @GetMapping(value = "searchevents")
    public List<Map<String, Object>> searchEventByEvent(@RequestParam String search) {
        String query = "SELECT * FROM event as eve JOIN discipline as dis ON eve.DISCIPLINEID = dis.DISCIPLINEID"
                + " JOIN units as un ON eve.UNITID = un.UNITID WHERE "
                + " eve.event LIKE '%" + search + "%'";
        System.out.println(dataAccess.run(query));
        //query = "SELECT * FROM schools";
        //System.out.println("response " + schoolService.search(query));
        return dataAccess.run(query);
    }

    @PostMapping(value = "saveevent", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public String saveDiscipline(@RequestParam String event, @RequestParam long disciplineId, @RequestParam long unitId) {
        Event eve = new Event();
        
        eve.setEvent(event);
        eve.setDiscipline(disRepo.findById(disciplineId).get());
        eve.setUnit(unitRepo.findById(unitId).get());

        System.out.println("saved");
        eveRepo.save(eve);
        return "saved";

    }

    @PostMapping(value = "updateevent", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public String updateDiscipline(@RequestParam String event, @RequestParam long eventId,@RequestParam long disciplineId, @RequestParam long unitId) {
        Event eve = new Event();
        eve = eveRepo.findById(eventId).get();
        eve.setEvent(event);
        eve.setDiscipline(disRepo.findById(disciplineId).get());
        eve.setUnit(unitRepo.findById(unitId).get());

        System.out.println("saved");
        eveRepo.save(eve);
        return "saved";

    }

}
