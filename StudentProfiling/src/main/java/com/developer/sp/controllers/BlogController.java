package com.developer.sp.controllers;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.developer.sp.config.MyUserDetails;
import com.developer.sp.models.Post;
import com.developer.sp.services.FileStorageService;
import com.developer.sp.services.PostService;
import com.developer.sp.services.UserService;
import com.developer.sp.util.AppConstants;



@RestController
public class BlogController {
	
	@Autowired
	private PostService postService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private FileStorageService fileStorageService;
	
	
	@GetMapping("/blog")
	public ModelAndView login() {
		ModelAndView mv = new ModelAndView("bloghome");
		return mv;
	}
        
        @GetMapping("/dashboard")
	public ModelAndView login1() {
		ModelAndView mv = new ModelAndView("dashboard");
		return mv;
	}
	
	@GetMapping("/create1")
	public ModelAndView create() {
		ModelAndView mv = new ModelAndView("createpost");
		return mv;
	}
	
	@GetMapping("/post")
	public ModelAndView singlePost() {
		ModelAndView mv = new ModelAndView("post");
		return mv;
	}
	
	@GetMapping("/createpost")
	public ModelAndView createPost() {
		ModelAndView mv = new ModelAndView("createpost");
		return mv;
	}
	
	@GetMapping(value = "post/{id}")
	public Post getPostById(@PathVariable Long id) {
		return postService.getPost(id);
	}
	
	@GetMapping(value = "/posts")
	public List<Post> posts(){
		return postService.getAllPosts();
	}
	
	@PostMapping(value = "/publish", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public String publishPost(@ModelAttribute Post post, @RequestParam("image") MultipartFile file) throws IOException{
		System.out.println("post");
		Post savePost = new Post();
		MyUserDetails userDetails =  (MyUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if(post.getCreationDate() == null) {
			post.setCreationDate(new Date());
		}
		String fileName = fileStorageService.storeFile(file);
		String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath().path(AppConstants.DOWNLOAD_PATH)
				.path(fileName).toUriString();
		
		savePost.setFeaturedImage(fileDownloadUri);
		savePost.setTitle(post.getTitle());
		savePost.setBody(post.getBody());
		savePost.setCreationDate(post.getCreationDate());
		System.out.println("post1");
		System.out.println(userDetails.getUsername());
		System.out.println(userService.getUser(userDetails.getUsername()));
		
		savePost.setCreator(userService.getUser(userDetails.getUsername()));
		System.out.println("post2");
		postService.insert(savePost);
		System.out.println(savePost);
		return "Post has been published";
	}
	@GetMapping(value = "/posts/{username}")
	public List<Post> postsByUser(@PathVariable String username){
		return postService.findByUser(userService.getUser(username));
	}
	/*
	@GetMapping(value = "downloadFile/{fileName}")
	public ResponseEntity<Resource> downloadFile(@PathVariable String fileName, HttpServletRequest request){
		Resource resource = fileStorageService.loadFileAsResource(fileName);
		String contentType = null;
		try {
			contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
		}catch(IOException ex) {
			ex.printStackTrace();
		}
		
		
		if(contentType == null) {
			contentType = AppConstants.DEFAULT_CONTENT_TYPE;
		}
		return ResponseEntity.ok().contentType(MediaType.parseMediaType(contentType))
				.header(HttpHeaders.CONTENT_DISPOSITION, String.format(AppConstants.FILE_DOWNLOAD_HTTP_HEADER, resource.getFilename()))
				.body(resource);
	}
*/
}
