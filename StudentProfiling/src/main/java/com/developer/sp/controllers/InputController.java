package com.developer.sp.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController
public class InputController {
	
	@GetMapping("/inputstatistics")
	public ModelAndView create() {
		ModelAndView mv = new ModelAndView("soccerstats");
		return mv;
	}
	
	@GetMapping("/admin3")
	public ModelAndView admin() {
		ModelAndView mv = new ModelAndView("admin");
		return mv;
	}

}
