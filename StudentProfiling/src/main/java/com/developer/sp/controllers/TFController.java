package com.developer.sp.controllers;



import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.developer.sp.models.TF_Cat;
import com.developer.sp.models.TrackField;
import com.developer.sp.repositories.TFCatRepo;
import com.developer.sp.services.TFService;
import org.springframework.web.bind.annotation.CrossOrigin;

@RestController
@CrossOrigin(origins = "*")
public class TFController {
	
	@Autowired
	private TFService tfService;
	
	@Autowired
	private TFCatRepo tfcRepo;
	
	@PostMapping(value = "savetfstat", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public String savStat(@RequestParam double personalBest, @RequestParam long eventId, @RequestParam long studentId){
		return tfService.saveStat(personalBest, studentId, eventId);
		
	}
	
	
	
	@GetMapping(value = "getcategories/{studentId}")
	public List<Map<String, Object>> searchCategories(@PathVariable Long studentId) {
		
		String query = "SELECT DISTINCT eve.event, dis.discipline, eve.eventid  FROM trackfield as tf JOIN  "
				+ "event as eve ON tf.EVENTID = eve.EVENTID JOIN "
				+ " discipline as dis ON eve.DISCIPLINEID = dis.DISCIPLINEID "
				+ " WHERE tf.studentid =  '" + studentId + "'";
		
		System.out.println("cat : " + tfService.search(query));
		//query = "SELECT * FROM schools";
		//System.out.println("response " + schoolService.search(query));
		return  tfService.search(query);
	}
	
	@GetMapping(value = "/getatheleterecords")
	public List<Map<String, Object>> getRecords(@RequestParam Long studentId, @RequestParam Long eventId) {
		
		String query = "SELECT eve.event, rc.worldbest, rc.nationalbest, rc.regionalbest, dis.discipline, tf.trackid, tf.datecollected, tf.personalbest, un.siunit  FROM trackfield as tf JOIN  "
				+ "event as eve ON tf.EVENTID = eve.EVENTID JOIN "
				+ " discipline as dis ON eve.DISCIPLINEID = dis.DISCIPLINEID JOIN "
                        + " records as rc ON rc.EVENT = eve.EVENTID JOIN"
                        + " units as un ON eve.UNITID = un.UNITID "
				+ " WHERE tf.STUDENTID =  '" + studentId + "' AND eve.EVENTID =  '" + eventId + "'";
		
		System.out.println("records : " + tfService.search(query));
		//query = "SELECT * FROM schools";
		//System.out.println("response " + schoolService.search(query));
		return  tfService.search(query);
	}
	
	@GetMapping(value = "/getatheleterecords1")
	public Map<String, Object> getRecordsForGraph(@RequestParam Long studentId, @RequestParam Long eventId) {
		
		String query = "SELECT eve.event, rc.worldbest, rc.nationalbest, rc.regionalbest, dis.discipline, tf.trackid, tf.datecollected, tf.personalbest, un.siunit  FROM trackfield as tf JOIN  "
				+ "event as eve ON tf.EVENTID = eve.EVENTID JOIN "
				+ " discipline as dis ON eve.DISCIPLINEID = dis.DISCIPLINEID  JOIN "
                        + " records as rc ON rc.EVENT = eve.EVENTID JOIN"
                        + " units as un ON eve.UNITID = un.UNITID "
				+ " WHERE tf.STUDENTID =  '" + studentId + "' AND eve.EVENTID =  '" + eventId + "'";
		
		System.out.println("records1 : " + tfService.search(query).get(0));
		//query = "SELECT * FROM schools";
		//System.out.println("response " + schoolService.search(query));
		return  tfService.search(query).get(0);
	}
	
	@GetMapping(value = "/getatheletevitals")
	public List<Map<String, Object>> getVitalss(@RequestParam Long studentId) {
		
		String query = "SELECT * FROM vitals "
				+ " WHERE studentid =  '" + studentId + "'";
		
		System.out.println("vitals : " + tfService.search(query));
		//query = "SELECT * FROM schools";
		//System.out.println("response " + schoolService.search(query));
		return  tfService.search(query);
	}

	@GetMapping(value = "getatheletedope")
	public List<Map<String, Object>> getDope(@RequestParam Long studentId) {
		
		String query = "SELECT * FROM antidope "
				+ " WHERE studentid =  '" + studentId + "'";
		
		System.out.println("dope : " + tfService.search(query));
		//query = "SELECT * FROM schools";
		//System.out.println("response " + schoolService.search(query));
		return  tfService.search(query);
	}


}
