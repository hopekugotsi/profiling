package com.developer.sp.controllers;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.developer.sp.dao.DataAccess;
import com.developer.sp.models.Province;
import com.developer.sp.repositories.ProvinceRepo;

@RestController
public class ProvinceCityContr {

	@Autowired
	private ProvinceRepo pRepo;
	
	@Autowired
	private DataAccess dataAccess;
	
	@GetMapping(value = "fetchprovincies")
	public List<Province> getProv() {
		System.out.println(pRepo.findAll());
		return pRepo.findAll();
	}
	
	@GetMapping(value = "searchcity")
	public List<Map<String, Object>> searchCity(@RequestParam String city, @RequestParam int province) {
		String query = "SELECT * FROM city WHERE "
				+ "city LIKE '%" + city + "%' AND "
						+ " provinceid = '" + province + "'";
		System.out.println(dataAccess.run(query));
		//query = "SELECT * FROM schools";
		//System.out.println("response " + schoolService.search(query));
		return dataAccess.run(query);
	}
}
