/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.developer.sp.controllers;

import com.developer.sp.config.MyUserDetails;
import com.developer.sp.dao.DataAccess;
import com.developer.sp.models.AntiDope;
import com.developer.sp.repositories.AntiDopeRepo;
import com.developer.sp.repositories.StudentRepo;
import com.developer.sp.services.UserService;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author hope
 */

@RestController
public class AntiDopeCtrl {
    
    @Autowired
    private AntiDopeRepo adRepo;
    
    @Autowired
	UserService userService;
        
        @Autowired
	StudentRepo studentRepo;
        
        @Autowired
	DataAccess dataAccess;
    
    @PostMapping(value = "saveantidopeinfor", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public String registerSchool(@RequestParam Long studentId, @RequestParam String dopeId, @RequestParam double heartrate, @RequestParam String dope, @RequestParam String adate1) throws ParseException{
		AntiDope ad;
		if(!dopeId.equals(" ")) {
                        System.out.println("dopeId defined");
			ad =(AntiDope)adRepo.findById(Long.parseLong(dopeId)).get();
		} else {
                        System.out.println("vitalsId undefined");
			ad = new AntiDope();
		}
		
		MyUserDetails userDetails =  (MyUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		
		Date parsed = format.parse(adate1);
		
		ad.setDateCol(parsed);
		
		
		
		ad.setCreator(userService.getUser(userDetails.getUsername()));
		ad.setStudent(studentRepo.findById(studentId).get());
		
		ad.setDope(dope);
                ad.setHeartRate(heartrate);
		
		adRepo.save(ad);
		return "saved";
		
	}
        
        @GetMapping(value = "/searchadbystdname")
	public List<Map<String, Object>> getVitalss(@RequestParam String search) {
		
		String query = "SELECT * FROM antidope as vt JOIN student as std ON vt.studentid = std.studentid "
				+ " WHERE (std.FIRSTNAME LIKE '%" + search + "%' OR std.MIDDLENAME LIKE '%" + search + "%' OR "
				+ " std.SURNAME LIKE '%" + search + "%')";
		
		System.out.println("vitals : " + dataAccess.run(query));
		//query = "SELECT * FROM schools";
		//System.out.println("response " + schoolService.search(query));
		return  dataAccess.run(query);
	}

}
