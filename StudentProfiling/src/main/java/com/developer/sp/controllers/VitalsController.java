/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.developer.sp.controllers;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.developer.sp.config.MyUserDetails;
import com.developer.sp.dao.DataAccess;
import com.developer.sp.models.Vitals;
import com.developer.sp.repositories.StudentRepo;
import com.developer.sp.repositories.VitalsRepo;
import com.developer.sp.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;

/**
 *
 * @author hope
 */

@RestController
public class VitalsController {
    
        @Autowired
	UserService userService;
        
        @Autowired
	StudentRepo studentRepo;
    
        @Autowired
	VitalsRepo vitalsRepo;
	
	@Autowired
	DataAccess dataAccess;
    
    @PostMapping(value = "savevitalsinfor", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public String registerSchool(@RequestParam Long studentId, @RequestParam String vitalId, @RequestParam double bmi, @RequestParam double height, @RequestParam double weight, @RequestParam String adate1) throws ParseException{
		Vitals vitals;
		if(!vitalId.equals(" ")) {
                        System.out.println("vitalsId defined");
			vitals =(Vitals)vitalsRepo.findById(Long.parseLong(vitalId)).get();
		} else {
                        System.out.println("vitalsId undefined");
			vitals = new Vitals();
		}
		
		MyUserDetails userDetails =  (MyUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		
		Date parsed = format.parse(adate1);
		
		vitals.setDateCol(parsed);
		
		
		
		vitals.setCreator(userService.getUser(userDetails.getUsername()));
		vitals.setStudent(studentRepo.findById(studentId).get());
		
		vitals.setBmi(bmi);
                vitals.setHeight(height);
                vitals.setWeight(weight);
		
		vitalsRepo.save(vitals);
		return "saved";
		
	}
        
        @GetMapping(value = "/searchvitalbystdname")
	public List<Map<String, Object>> getVitalss(@RequestParam String search) {
		
		String query = "SELECT * FROM vitals as vt JOIN student as std ON vt.studentid = std.studentid "
				+ " WHERE (std.FIRSTNAME LIKE '%" + search + "%' OR std.MIDDLENAME LIKE '%" + search + "%' OR "
				+ " std.SURNAME LIKE '%" + search + "%')";
		
		System.out.println("vitals : " + dataAccess.run(query));
		//query = "SELECT * FROM schools";
		//System.out.println("response " + schoolService.search(query));
		return  dataAccess.run(query);
	}
    
}
