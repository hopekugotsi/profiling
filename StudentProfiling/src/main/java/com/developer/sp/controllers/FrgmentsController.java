package com.developer.sp.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController
public class FrgmentsController {
	
	@GetMapping("/fragments/saveschool")
	public ModelAndView saveSchool() {
		ModelAndView mv = new ModelAndView("/fragments/saveschool");
		return mv;
	}
	
	@GetMapping("/fragments/savestudent")
	public ModelAndView saveStudent() {
		ModelAndView mv = new ModelAndView("/fragments/savestudent");
		return mv;
	}
	
	@GetMapping("/fragments/savesoccerstats")
	public ModelAndView saveSoccerStats() {
		ModelAndView mv = new ModelAndView("/fragments/soccerstats");
		return mv;
	}

}
