package com.developer.sp.controllers;

import com.developer.sp.models.TF_Cat;
import com.developer.sp.repositories.CategoryRepository;
import com.developer.sp.repositories.TFCatRepo;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CategoryController {
	
	@Autowired
	private CategoryRepository catRepo;
        
        @Autowired
	private TFCatRepo tfcRepo;

	@PostMapping(value = "savecategory")
	public String saveCat(@RequestBody TF_Cat cat){
		
		cat.setRegDate(new Date());
		
		
		System.out.println("saved");
		catRepo.save(cat);
		return "saved";
		
	}
    
	@PostMapping(value = "updatecategory")
	public String updateCat(@RequestBody TF_Cat cat){
		
		TF_Cat updateCat = new TF_Cat();
		updateCat = catRepo.findById(cat.getCatId()).get();
		updateCat.setName(cat.getName());
		updateCat.setRegDate(new Date());
		System.out.println("saved");
		catRepo.save(updateCat);

		return "saved";
		
	}
        
        @GetMapping(value = "fetchcategory")
	public List<TF_Cat> searchCat() {
		System.out.println(tfcRepo.findAll());
		//query = "SELECT * FROM schools";
		//System.out.println("response " + schoolService.search(query));
		return tfcRepo.findAll();
	}
    
}