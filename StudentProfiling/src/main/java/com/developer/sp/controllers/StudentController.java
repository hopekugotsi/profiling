package com.developer.sp.controllers;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.developer.sp.models.Student;
import com.developer.sp.services.StudentService;
import org.springframework.web.bind.annotation.CrossOrigin;

@RestController
@CrossOrigin(origins = "*")
public class StudentController {
	
	@Autowired
	private StudentService studentService;
	
	@PostMapping(value = "/admin/registerstudent", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public String registerSchool(@ModelAttribute Student student,@RequestParam Long schoolId, @RequestParam String dob1, @RequestParam Long city, @RequestParam("image") MultipartFile file) throws IOException, ParseException{
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		System.out.println("student");
		Date parsed = format.parse(dob1);
		student.setDob(parsed);
		return studentService.saveStudent(student,schoolId, city, file);
		
	}
	
	
	
	@GetMapping(value = "getstudentsbyschoolid/{schoolId}")
	public List<Map<String, Object>> getStudentsBySchoolId(@PathVariable Long schoolId) {
		String query = "SELECT * FROM student WHERE "
				+ "schoolId LIKE '%" + schoolId + "%'";
		return studentService.getStudentsBySchoolId(query);
	}
	
	@GetMapping(value = "getstudents")
	public List<Student> getSchools(@RequestParam String email) {
		return studentService.getStudents();
	}
	
	@GetMapping(value = "searchstudents")
	public List<Map<String, Object>> searchStudents(@RequestParam String search) {
		String query = "SELECT * FROM student WHERE "
				+ "FIRSTNAME LIKE '%" + search + "%'";
		System.out.println(studentService.search(query));
		//query = "SELECT * FROM schools";
		//System.out.println("response " + schoolService.search(query));
		return studentService.search(query);
	}
	//@SuppressWarnings("unchecked")
	@GetMapping(value = "getstudent/{id}")
	public Map<String, Object> getSchool(@PathVariable Long id) {
		//return studentService.getStudent(id);
		String query = "SELECT * FROM student as std JOIN vitals as vt ON " + 
				" std.studentid = vt.studentid WHERE "
				+ "std.studentid LIKE '%" + id + "%'";
		System.out.println("return : " + studentService.search(query));
		
		List<Map<String, Object>> result = studentService.search(query);
		
		Map<String, Object> result1 = new HashMap<>();
		Object error = "no vitals information for athlete";
		if (result.isEmpty()) {
			return (Map<String, Object>) result1.put("error", error);
		}
		else {
			return studentService.search(query).get(0);
		}
		
	}
	@GetMapping(value = "searchstudentshome")
	public List<Map<String, Object>> search(@RequestParam String name,@RequestParam String school, @RequestParam String city ) {
		/*
		String query = "SELECT * FROM schools WHERE "
				+ "name LIKE '%" + search + "%'";*/
		//query = "SELECT * FROM schools";
		String query = "SELECT std.studentid, std.firstname, std.middlename, std.surname, std.dob, sch.name, ct.city"
				+ "  FROM schools as sch JOIN  "
				+ "student as std ON std.SCHOOLID = sch.SCHOOLID JOIN "
				+ " city as ct ON std.CITY = ct.CITYID "
				/*+ "trackfield as tf ON std.STUDENTID = tf.STUDENTID JOIN "
				+ "event as eve ON tf.EVENTID = eve.EVENTID JOIN " + 
				" discipline as dis ON eve.DISCIPLINEID = dis.DISCIPLINEID "*/
				+ " WHERE  sch.NAME LIKE  '%" + school + "%' AND (std.FIRSTNAME LIKE '%" + name + "%' OR std.MIDDLENAME LIKE '%" + name + "%' OR "
				+ " std.SURNAME LIKE '%" + name + "%') AND ct.city LIKE '%" + city + "%'";
		
		List<Map<String, Object>> result = studentService.search(query);
		
		
		
		System.out.println("response " + studentService.search(query));
		return studentService.search(query);
	}


}
