/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.developer.sp.controllers;

import com.developer.sp.dao.DataAccess;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author hope
 */
@RestController
public class ViewsController {

    @Autowired
    DataAccess dataAccess;

    @GetMapping(value = "views/allathletes")
    public List<Map<String, Object>> athletes(@RequestParam String name) {
        
        String query = "SELECT std.firstname, std.middlename, std.surname, std.dob, sch.name, ct.city, eve.event, dis.discipline "
                + "  FROM schools as sch JOIN  "
                + "student as std ON std.SCHOOLID = sch.SCHOOLID JOIN "
                + " city as ct ON std.CITY = ct.CITYID JOIN "
                + "trackfield as tf ON std.STUDENTID = tf.STUDENTID JOIN "
                + "event as eve ON tf.EVENTID = eve.EVENTID JOIN " + 
				" discipline as dis ON eve.DISCIPLINEID = dis.DISCIPLINEID "
                + " WHERE  std.FIRSTNAME LIKE '%" + name + "%' OR std.MIDDLENAME LIKE '%" + name + "%' OR "
                + " std.SURNAME LIKE '%" + name + "%'";

        List<Map<String, Object>> result = dataAccess.run(query);
        return result;
    }
    
    @GetMapping(value = "views/allschools")
    public List<Map<String, Object>> schools(@RequestParam String search) {
		/*
		String query = "SELECT * FROM schools WHERE "
				+ "name LIKE '%" + search + "%'";*/
		//query = "SELECT * FROM schools";
		String query = "SELECT sch.name, sch.est, prv.province, ct.city  FROM schools as sch JOIN  "
				+ "city as ct ON sch.CITY = ct.CITYID JOIN "
				+ " province as prv ON ct.PROVINCEID = prv.PROVINCEID "
				+ " WHERE  sch.NAME LIKE  '%" + search + "%'";
		
		
		return dataAccess.run(query);
	}
    
    @GetMapping(value = "views/allevents")
    public List<Map<String, Object>> events(@RequestParam String search) {
		/*
		String query = "SELECT * FROM schools WHERE "
				+ "name LIKE '%" + search + "%'";*/
		//query = "SELECT * FROM schools";
		String query = "SELECT eve.event, dis.discipline, cat.name  FROM tf_cat as cat JOIN  "
				+ "discipline as dis ON cat.catid = dis.catid JOIN "
				+ " event as eve ON eve.disciplineid = dis.disciplineid "
				+ " WHERE  eve.event LIKE  '%" + search + "%'";
		
		
		return dataAccess.run(query);
	}


}
