package com.developer.sp.controllers;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.developer.sp.config.MyUserDetails;
import com.developer.sp.dao.DataAccess;
import com.developer.sp.models.AthGallery;
import com.developer.sp.models.Coach;
import com.developer.sp.repositories.AthGRepo;
import com.developer.sp.repositories.SchoolRepo;
import com.developer.sp.repositories.StudentRepo;
import com.developer.sp.services.FileStorageService;
import com.developer.sp.services.UserService;
import com.developer.sp.util.AppConstants;
import org.springframework.web.bind.annotation.CrossOrigin;


@RestController
@CrossOrigin(origins = "*")
public class AthGController {

	@Autowired
	private AthGRepo agRepo;
	
	@Autowired
	private FileStorageService fileStorageService;
	
	@Autowired
	UserService userService;
	
	@Autowired
	StudentRepo studentRepo;
	
	@Autowired
	DataAccess dataAccess;
	
	@PostMapping(value = "addgallery", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public String registerSchool(@RequestParam Long studentId, @RequestParam String description, @RequestParam("image") MultipartFile file) throws IOException{
		AthGallery gallery = new AthGallery();
		
		MyUserDetails userDetails =  (MyUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		gallery.setDateUploaded(new Date());
		
		String fileName = fileStorageService.storeFile(file);
		String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath().path(AppConstants.DOWNLOAD_PATH)
				.path(fileName).toUriString();
		
		gallery.setLink(fileDownloadUri);
		gallery.setCreator(userService.getUser(userDetails.getUsername()));
		gallery.setStudent(studentRepo.findById(studentId).get());
		
		gallery.setDescription(description);
		
		agRepo.save(gallery);
		return "saved";
		
	}
	@GetMapping(value = "getathgallery/{studentId}")
	public List<Map<String, Object>> getStudentsBySchoolId(@PathVariable Long studentId) {
		String query = "SELECT link FROM athgallery WHERE "
				+ "studentid = '" + studentId + "'";
		return dataAccess.run(query);
	}
        /*
        @GetMapping(value = "getathgallery/{studentId}")
	public List<Map<String, Object>> getStudentsBySchoolId(@PathVariable Long studentId) {
		String query = "SELECT * FROM athgallery WHERE "
				+ "studentid = '" + studentId + "'";
		return dataAccess.run(query);
	}
*/
}
