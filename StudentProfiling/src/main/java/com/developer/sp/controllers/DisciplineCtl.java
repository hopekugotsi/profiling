package com.developer.sp.controllers;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.developer.sp.dao.DataAccess;
import com.developer.sp.models.Discipline;
import com.developer.sp.models.TF_Cat;
import com.developer.sp.repositories.CategoryRepository;
import com.developer.sp.repositories.DisciplineRepository;
import java.util.Date;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@RestController
public class DisciplineCtl {

    @Autowired
    private DataAccess dataAccess;
    
    @Autowired
    private CategoryRepository catRepo;
    
    @Autowired
    private DisciplineRepository disRepo;

    @GetMapping(value = "searchdiscipline")
    public List<Map<String, Object>> searchDisciplineByCategory(@RequestParam String discipline, @RequestParam int category) {
        String query = "SELECT * FROM discipline WHERE "
                + "discipline LIKE '%" + discipline + "%' AND "
                + " catId = '" + category + "'";
        System.out.println(dataAccess.run(query));
        //query = "SELECT * FROM schools";
        //System.out.println("response " + schoolService.search(query));
        return dataAccess.run(query);
    }
    
    @GetMapping(value = "searchdisciplines")
    public List<Map<String, Object>> searchDiscipline(@RequestParam String search) {
        String query = "SELECT * FROM discipline as dis JOIN tf_cat as cat ON dis.catId = cat.catId WHERE "
                + "discipline LIKE '%" + search + "%'";
        System.out.println(dataAccess.run(query));
        //query = "SELECT * FROM schools";
        //System.out.println("response " + schoolService.search(query));
        return dataAccess.run(query);
    }

    @PostMapping(value = "savediscipline", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public String saveDiscipline(@RequestParam String discipline, @RequestParam Long catId1) {
        Discipline dis = new Discipline();

        dis.setDiscipline(discipline);
        
        dis.setCategory(catRepo.findById(catId1).get());

        System.out.println("saved");
        disRepo.save(dis);
        return "saved";

    }

    @PostMapping(value = "updatediscipline", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public String updateDiscipline(@RequestParam String discipline, @RequestParam long catId, @RequestParam long disId) {
        Discipline dis = new Discipline();
        dis = disRepo.findById(disId).get();
        dis.setDiscipline(discipline);
        dis.setCategory(catRepo.findById(catId).get());

        System.out.println("saved");
        disRepo.save(dis);
        return "saved";

    }
}
