/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.developer.sp.controllers;

import com.developer.sp.dao.DataAccess;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author hope
 */
@RestController
public class UnitController {

    @Autowired
    private DataAccess dataAccess;

    @GetMapping(value = "searchunits")
    public List<Map<String, Object>> searchUnit() {
        String query = "SELECT * FROM units";
        System.out.println(dataAccess.run(query));
        //query = "SELECT * FROM schools";
        //System.out.println("response " + schoolService.search(query));
        return dataAccess.run(query);
    }

}
