package com.developer.sp.controllers;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.developer.sp.config.MyUserDetails;
import com.developer.sp.dao.DataAccess;
import com.developer.sp.models.AthMedals;
import com.developer.sp.models.Medals;
import com.developer.sp.models.SchMedals;
import com.developer.sp.repositories.AthMedalsRepo;
import com.developer.sp.repositories.MedalRepo;
import com.developer.sp.repositories.SchMedalsRepo;
import com.developer.sp.repositories.SchoolRepo;
import com.developer.sp.repositories.StudentRepo;
import com.developer.sp.services.UserService;
import java.util.ArrayList;
import org.springframework.web.bind.annotation.CrossOrigin;

@RestController
@CrossOrigin(origins = "*")
public class SchMController {
	
	@Autowired
	UserService userService;
	
	@Autowired
	SchMedalsRepo smRepo;
	
	@Autowired
	MedalRepo medalRepo;
	
	@Autowired
	SchoolRepo schoolRepo;
	
	@Autowired
	DataAccess dataAccess;
	
	@PostMapping(value = "addschmedal", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public String registerSchool(@RequestParam Long schoolId, @RequestParam Long medalId, @RequestParam String description, @RequestParam String adate1) throws ParseException{
		SchMedals medals = new SchMedals();
		
		MyUserDetails userDetails =  (MyUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		
		Date parsed = format.parse(adate1);
		medals.setAdate(parsed);
		
		
		
		medals.setCreator(userService.getUser(userDetails.getUsername()));
		
		System.out.println("test1");
		medals.setSchool(schoolRepo.findById(schoolId).get());
		System.out.println(medalId + "test2");
		medals.setMedal(medalRepo.findById(medalId).get());
		System.out.println("test3");
		medals.setDescription(description);
		
		smRepo.save(medals);
		return "saved";
		
	}
	
	@GetMapping(value = "getschgold/{schoolId}")
	public List<Map<String, Object>> getGold(@PathVariable Long schoolId) {
		String query = "SELECT * FROM schmedals WHERE "
				+ "schoolId = '" + schoolId + "' AND medalid = '" + 1 + "'";
		return dataAccess.run(query);
	}
	
	@GetMapping(value = "getschsilver/{schoolId}")
	public List<Map<String, Object>> getSilver(@PathVariable Long schoolId) {
		String query = "SELECT * FROM schmedals WHERE "
				+ "schoolId = '" + schoolId + "' AND medalid = '" + 2 + "'";
		return dataAccess.run(query);
	}
	
	@GetMapping(value = "getschbronze/{schoolId}")
	public List<Map<String, Object>> getBronze(@PathVariable Long schoolId) {
		String query = "SELECT * FROM schmedals WHERE "
				+ "schoolId = '" + schoolId + "' AND medalid = '" + 3 + "'";
		return dataAccess.run(query);
	}
	
	@GetMapping(value = "getmedals")
	public List<Medals> getBronze() {
		System.out.println(medalRepo.findAll());
		return medalRepo.findAll();
	}
        
        @GetMapping(value = "getschmedals/{schoolId}")
	public List<Map<String, Object>> getMedals(@PathVariable Long schoolId) {
		
                List<Map<String, List<Map<String, Object>>>> val = new ArrayList();
                Map<String, List<Map<String, Object>>> value;
                
                String query = "SELECT description, awarddate, medalid FROM schmedals WHERE "
				+ "schoolid = '" + schoolId + "'";
                
                               System.out.println("medals " + dataAccess.run(query));
		return dataAccess.run(query);
	}


}
