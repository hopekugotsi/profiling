package com.developer.sp.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController
public class MainController {
	
	@GetMapping("/")
	public ModelAndView login() {
		ModelAndView mv = new ModelAndView("redirect:/admin");
		return mv;
	}
	/*
	@GetMapping("/home")
	public ModelAndView home() {
		ModelAndView mv = new ModelAndView("home");
		return mv;
	}
	
	
	@GetMapping("/soccer")
	public ModelAndView soccer() {
		ModelAndView mv = new ModelAndView("soccer");
		return mv;
	}
	
	@GetMapping("/admin1")
	public ModelAndView admin1() {
		ModelAndView mv = new ModelAndView("admin2");
		return mv;
	}
	*/
	@GetMapping("/admin")
	public ModelAndView admin3() {
		ModelAndView mv = new ModelAndView("admin3");
		return mv;
	}
	/*
	@GetMapping("/school/{schoolId}")
	public ModelAndView school(@PathVariable("schoolId") long id) {
		ModelAndView mv = new ModelAndView("school");
		return mv;
	}
	
	@GetMapping("/profile")
	public ModelAndView profile() {
		ModelAndView mv = new ModelAndView("profile");
		return mv;
	}
	@GetMapping("/shop")
	public ModelAndView shop() {
		ModelAndView mv = new ModelAndView("shophome");
		return mv;
	}
	
	@GetMapping("/school")
	public ModelAndView school() {
		ModelAndView mv = new ModelAndView("school");
		return mv;
	}
	
	@GetMapping("/athlete/{schoolId}")
	public ModelAndView student() {
		ModelAndView mv = new ModelAndView("student");
		return mv;
	}
	
	@GetMapping("/linechart")
	public ModelAndView lineChart() {
		ModelAndView mv = new ModelAndView("linechart");
		return mv;
	}
	
	@GetMapping("/barchart")
	public ModelAndView barChart() {
		ModelAndView mv = new ModelAndView("bargraph");
		return mv;
	}
	
	*/
}
