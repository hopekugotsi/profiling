package com.developer.sp.controllers;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.developer.sp.config.MyUserDetails;
import com.developer.sp.dao.DataAccess;
import com.developer.sp.models.AthGallery;
import com.developer.sp.models.SchGallery;
import com.developer.sp.repositories.AthGRepo;
import com.developer.sp.repositories.SchGRepo;
import com.developer.sp.repositories.SchoolRepo;
import com.developer.sp.repositories.StudentRepo;
import com.developer.sp.services.FileStorageService;
import com.developer.sp.services.UserService;
import com.developer.sp.util.AppConstants;
import org.springframework.web.bind.annotation.CrossOrigin;

@RestController
@CrossOrigin(origins = "*")
public class SchGController {

	@Autowired
	private SchGRepo sgRepo;
	
	@Autowired
	private FileStorageService fileStorageService;
	
	@Autowired
	UserService userService;
	
	@Autowired
	SchoolRepo schoolRepo;
	
	@Autowired
	DataAccess dataAccess;
	
	@PostMapping(value = "addschoolgallery", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public String registerSchool(@RequestParam Long schoolId, @RequestParam String description, @RequestParam("image") MultipartFile file) throws IOException{
		SchGallery gallery = new SchGallery();
		
		MyUserDetails userDetails =  (MyUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		gallery.setDateUploaded(new Date());
		
		String fileName = fileStorageService.storeFile(file);
		String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath().path(AppConstants.DOWNLOAD_PATH)
				.path(fileName).toUriString();
		
		gallery.setLink(fileDownloadUri);
		gallery.setCreator(userService.getUser(userDetails.getUsername()));
		gallery.setSchool(schoolRepo.findById(schoolId).get());
		
		gallery.setDescription(description);
		
		sgRepo.save(gallery);
		return "saved";
		
	}
	
	@GetMapping(value = "getschoolimages/{schoolId}")
	public List<Map<String, Object>> getStudentsBySchoolId(@PathVariable Long schoolId) {
		String query = "SELECT link FROM schgallery WHERE "
				+ "schoolid = '" + schoolId + "'";
		return dataAccess.run(query);
	}
        /*
        @GetMapping(value = "getschoolimages/{schoolId}")
	public List<Map<String, Object>> getStudentsBySchoolId(@PathVariable Long schoolId) {
		String query = "SELECT * FROM schgallery WHERE "
				+ "school_Id = '" + schoolId + "'";
		return dataAccess.run(query);
	}
*/
}
