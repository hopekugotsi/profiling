package com.developer.sp.controllers;


import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;


import com.developer.sp.models.Comments;

import com.developer.sp.repositories.CommentsRepo;

@RestController
public class CommentsController {
	
	@Autowired
	private CommentsRepo cRepo;
	
	@PostMapping(value = "savecomment")
	public String registerSchool(@RequestBody Comments comment){
		//SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		//school = new School();
		//Date parsed = format.parse(est1);
		//school.setEst(parsed);
		
		comment.setCdate(new Date());
		//System.out.println("saving  " + school + " " + city);
		
		cRepo.save(comment);
		return "saved";
		
	}

}
