package com.developer.sp.controllers;

import com.developer.sp.models.Records;
import com.developer.sp.repositories.EventRepo;
import com.developer.sp.repositories.RecordRepository;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RecordController {

    @Autowired
    private RecordRepository recordRepo;

    @Autowired
    private EventRepo eveRepo;

    @PostMapping(value = "saverecord")
    public String saveRecord(@ModelAttribute Records record, @RequestParam Long event) throws ParseException {
        //SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

        //Date parsed = format.parse(dateSet);
        record.setDateSet(new Date());
        record.setEvent(eveRepo.findById(event).get());
        record.setIsRecent(true);
        recordRepo.save(record);

        return "saved";

    }

    @PostMapping(value = "updaterecord")
    public String updateRecord(@ModelAttribute Records record, @RequestParam Long event, @RequestParam String dateSet) throws ParseException {
        Records record1 = new Records();
        record1 = recordRepo.findById(record.getRecordId()).get();
        
        record1.setEvent(eveRepo.findById(event).get());
        record1.setNatBest(record.getNatBest());
        record1.setRegBest(record.getRegBest());
        record1.setWorldBest(record.getWorldBest());
        
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

        Date parsed = format.parse(dateSet);
        record1.setDateSet(parsed);
        /*
		Records recent = new Records();
		recent = recordRepo.findRecentRecord(record.event);
		recent.setIsRecent(false);*/
      
        

        recordRepo.save(record);
        
        recordRepo.updateRecentRecord(eveRepo.findById(event).get());

        return "saved";

    }

    @GetMapping(value = "getrecordbyeventid")
    public Records getRecordsByEvent(@RequestParam Long event) {

        return recordRepo.findRecentRecord(eveRepo.findById(event).get());
    }

    @GetMapping(value = "getrecordbyeventname")
    public List<Map<String, Object>> getRecordsByEventName(@RequestParam String event) {
        System.out.println(recordRepo.findRecentRecordByEventName(event));
        return recordRepo.findRecentRecordByEventName(event);
    }
}
