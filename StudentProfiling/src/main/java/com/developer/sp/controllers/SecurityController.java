package com.developer.sp.controllers;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.developer.sp.models.Admin;
import com.developer.sp.services.UserService;
import com.developer.sp.util.AppConstants;
import com.developer.sp.services.FileStorageService;


@RestController
public class SecurityController {
	
	@Autowired
	UserService userService;
	
	@Autowired
	private FileStorageService fileStorageService;
	
	@GetMapping("/login")
	public ModelAndView login() {
		ModelAndView mv = new ModelAndView("login");
		return mv;
	}
	
	@RequestMapping(value = "/register", method = RequestMethod.GET)
	public ModelAndView register() {
		ModelAndView mv = new ModelAndView("register");
		Admin user = new Admin();
		mv.addObject("user", user);
		mv.setViewName("register");
		return mv;
	}
	
	@RequestMapping(value = "/registeruser", method = RequestMethod.POST)
	public ModelAndView registerUser(@Valid Admin user, BindingResult bindResult, ModelMap modelmap) {
                System.out.println("registering new user");
		ModelAndView mv = new ModelAndView();
		if(bindResult.hasErrors()) {
			mv.addObject("successMessage", "please correct the errors in form");
			System.out.println("bindingResult :" + bindResult);
		}
		else if(userService.isUserPresent(user)){
			mv.addObject("successMesaage", "user is already present");
		}
		else {
			userService.saveUser(user);
			mv.addObject("successMesaage", "user created successfully");
			System.out.println(user + "created successfully");
		}
		mv.addObject("user", user);
		mv.setViewName("login");
		return mv;
	}

}
