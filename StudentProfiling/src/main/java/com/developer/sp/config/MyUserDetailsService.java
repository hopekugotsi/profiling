package com.developer.sp.config;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.developer.sp.models.Admin;
import com.developer.sp.repositories.AdminRepository;


@Service
public class MyUserDetailsService implements UserDetailsService{
	@Autowired
	AdminRepository userRepository;
	
	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException{
		Optional<Admin> user =  userRepository.findByEmail(email); 
		
		user.orElseThrow(() -> new UsernameNotFoundException("Not Found" + email));
		return user.map(MyUserDetails::new).get();
	}
}
