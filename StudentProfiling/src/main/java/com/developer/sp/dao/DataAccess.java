package com.developer.sp.dao;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class DataAccess {

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	public List<Map<String, Object>> run(String query){
		List<Map<String, Object>> schools = jdbcTemplate.queryForList(query);
		return schools;
	}
}
