/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.developer.sp.exception;

import lombok.Data;
import org.springframework.stereotype.Component;

/**
 *
 * @author hope
 */
@Component
@Data
public class CustomException  extends Exception{
    private static final long serialVersionUID = 1L;
    
    private int code;
    private String message;
}
