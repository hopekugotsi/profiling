/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.developer.sp.exception;

/**
 *
 * @author hope
 */
public class ExceptionThrower {
    
    public void throwGeneralException() throws Exception{
        Exception e = new Exception("Error from general Exception");
        throw e;
    }
    
    public void throwCustomException(int code, String message) throws CustomException{
        
        CustomException e = new CustomException();
        e.setCode(code);
        e.setMessage(message);
        
        throw e; 
    }
    
}
