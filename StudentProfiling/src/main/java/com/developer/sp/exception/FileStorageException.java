package com.developer.sp.exception;

public class FileStorageException extends RuntimeException{
	private static final long versionUID = 1L;

	public FileStorageException(String message) {
		super(message);
	}
	
	public FileStorageException(String message, Throwable cause) {
		super(message,cause);
	}
}
