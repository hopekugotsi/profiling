/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.developer.sp.exception;

import com.developer.sp.models.AppResponse;
import java.util.HashMap;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.NoHandlerFoundException;

/**
 *
 * @author hope
 */
@RestControllerAdvice
public class ExceptionController {

    @ExceptionHandler(NoHandlerFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public HashMap<String, String> handleNoHandlerFound(NoHandlerFoundException e, WebRequest request) {
        HashMap<String, String> response = new HashMap<>();
        response.put("status", "fail");
        response.put("message", e.getLocalizedMessage());
        return response;
    }
    
    @ExceptionHandler(Exception.class)
    public final ResponseEntity<AppResponse> somethingWentWrong(Exception e){
        AppResponse appResponse = new AppResponse();
        appResponse.setCode(HttpStatus.BAD_REQUEST.value());
        appResponse.setMessage(e.getMessage());
        return new ResponseEntity<AppResponse>(appResponse, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);  
    }
    
    @ExceptionHandler(CustomException.class)
    public ResponseEntity<AppResponse> specialException(CustomException e){
        AppResponse appResponse = new AppResponse();
        appResponse.setCode(HttpStatus.BAD_REQUEST.value());
        appResponse.setMessage(e.getMessage());
        
        return new ResponseEntity<AppResponse>(appResponse, HttpStatus.BAD_REQUEST);
    }
}