package com.developer.sp.services;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.developer.sp.dao.DataAccess;
import com.developer.sp.models.TrackField;
import com.developer.sp.repositories.EventRepo;
import com.developer.sp.repositories.TFCatRepo;
import com.developer.sp.repositories.TFRepo;

@Service
public class TFServiceImpl implements TFService{
	
	@Autowired
	private TFRepo tfRepo;
	
	@Autowired
	private DataAccess dataAccess;
	
	@Autowired
	private StudentService studentService;
	
	@Autowired
	private EventRepo eventRepo;

	@Override
	public String saveStat(double personalBest, long studentId, long eventId) {
		TrackField tf = new TrackField();
                
		tf.setPersonalBest(personalBest);
		tf.setDateCollected(new Date());
		
		tf.setEvent(eventRepo.getOne(eventId));
		
		tf.setStudent(studentService.getStudent(studentId));
		
		//tf.setCategory(tfcRepo.findById(catId).get());
		
		tfRepo.save(tf);
		// TODO Auto-generated method stub
		
		return "saved";
	}

	@Override
	public TrackField getStat() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<TrackField> getStats() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Map<String, Object>> search(String query) {
		// TODO Auto-generated method stub
		return dataAccess.run(query);
	}

}
