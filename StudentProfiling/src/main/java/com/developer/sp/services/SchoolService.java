package com.developer.sp.services;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.springframework.web.multipart.MultipartFile;

import com.developer.sp.models.School;

public interface SchoolService {
	
	public String saveSchool(School school,long city, MultipartFile file) throws IOException;
	
	public School getSchool(String email);
	
	public School getSchoolById(Long id);

	public List<School> getSchools();
	
	public List<Map<String, Object>> search(String query);

}
