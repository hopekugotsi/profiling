package com.developer.sp.services;

import com.developer.sp.models.Admin;

public interface UserService {
	public void saveUser(Admin user);
	
	public boolean isUserPresent(Admin user);
	
	public Admin getUser(String email);
}
