package com.developer.sp.services;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.developer.sp.dao.DataAccess;
import com.developer.sp.models.SoccerStatistics;
import com.developer.sp.repositories.SoccerRepo;

@Service
public class SoccerServiceImpl implements SoccerService {
	
	@Autowired
	private SoccerRepo soccerRepo;
	
	@Autowired
	private DataAccess dataAccess;


	@Override
	public String saveStat(SoccerStatistics soccer) {
		soccer.setCreationDate(new Date());
		
		soccerRepo.save(soccer);
		return "saved";
	}

	@Override
	public SoccerStatistics getStat() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<SoccerStatistics> getStats() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Map<String, Object>> search(String query) {
		// TODO Auto-generated method stub
		return null;
	}

}
