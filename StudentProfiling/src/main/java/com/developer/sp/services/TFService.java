package com.developer.sp.services;

import java.util.List;
import java.util.Map;


import com.developer.sp.models.TrackField;

public interface TFService {
	
	public String saveStat(double personalBest, long studentId, long catId);
	
	public TrackField getStat();
	
	public List<TrackField> getStats();

	public List<Map<String, Object>> search(String query);


}
