package com.developer.sp.services;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.springframework.web.multipart.MultipartFile;

import com.developer.sp.models.Student;

public interface StudentService {
	
	public String saveStudent(Student student,long schoolId, long city, MultipartFile file) throws IOException;
	
	public Student getStudent(Long id);

	public List<Student> getStudents();
	
	public List<Map<String, Object>> search(String query);
	
	public List<Map<String, Object>> getStudentsBySchoolId(String query);


}
