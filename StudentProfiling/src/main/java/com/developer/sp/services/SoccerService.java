package com.developer.sp.services;

import java.util.List;
import java.util.Map;

import com.developer.sp.models.SoccerStatistics;

public interface SoccerService {
	
	public String saveStat(SoccerStatistics soccer);
	
	public SoccerStatistics getStat();
	
	public List<SoccerStatistics> getStats();

	public List<Map<String, Object>> search(String query);
}
