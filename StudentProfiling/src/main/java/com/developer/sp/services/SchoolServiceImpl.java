package com.developer.sp.services;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.developer.sp.config.MyUserDetails;
import com.developer.sp.dao.DataAccess;
import com.developer.sp.models.School;
import com.developer.sp.repositories.CityRepo;
import com.developer.sp.repositories.SchoolRepo;
import com.developer.sp.util.AppConstants;

@Service
public class SchoolServiceImpl implements SchoolService {
	
	@Autowired
	SchoolRepo schoolRepo;
	
	@Autowired
	private FileStorageService fileStorageService;
	
	@Autowired
	UserService userService;
	
	@Autowired
	private DataAccess dataAccess;
	
	@Autowired
	private CityRepo cityRepo;

	@Override
	public String saveSchool(School school, long city, MultipartFile file)  throws IOException{
		//school = new School();
		System.out.println("saved");
		MyUserDetails userDetails =  (MyUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		school.setDateCreated(new Date());
		
		String fileName = fileStorageService.storeFile(file);
		String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath().path(AppConstants.DOWNLOAD_PATH)
				.path(fileName).toUriString();
		
		school.setProfileImg(fileDownloadUri);
		school.setCreator(userService.getUser(userDetails.getUsername()));
		school.setCity(cityRepo.getOne(city));
		
		System.out.println("saved");
		schoolRepo.save(school);
		
		return "saved";
	}

	@Override
	public School getSchool(String username) {
		return schoolRepo.findByUsername(username).get();
		
	}
	@Override
	public School getSchoolById(Long id) {
		return schoolRepo.findById(id).get();
		
	}

	@Override
	public List<School> getSchools() {
		// TODO Auto-generated method stub
		return schoolRepo.findAll();
	}

	@Override
	public List<Map<String, Object>> search(String query) {
		// TODO Auto-generated method stub
		return dataAccess.run(query);
	}

}
