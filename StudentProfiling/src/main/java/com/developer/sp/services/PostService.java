package com.developer.sp.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.developer.sp.models.Admin;
import com.developer.sp.models.Post;
import com.developer.sp.models.User;
import com.developer.sp.repositories.PostRepo;

@Service
public class PostService {
	
	@Autowired
	private PostRepo postRepo;
	
	public List<Post> getAllPosts(){
		return postRepo.findAll();
	}
	
	public void insert(Post post) {
		postRepo.save(post);
	}
	
	public Post getPost(Long id) {
		return postRepo.findById(id).get();
	}
	
	public List<Post> findByUser(Admin admin){
		return postRepo.findByCreatorId(admin.getId());
	}

}
