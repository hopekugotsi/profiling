package com.developer.sp.services;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.developer.sp.config.MyUserDetails;
import com.developer.sp.dao.DataAccess;
import com.developer.sp.models.Student;
import com.developer.sp.repositories.CityRepo;
import com.developer.sp.repositories.SchoolRepo;
import com.developer.sp.repositories.StudentRepo;
import com.developer.sp.util.AppConstants;

@Service
public class StudentServiceImpl implements StudentService{
	
	@Autowired
	StudentRepo studentRepo;
	
	@Autowired
	private FileStorageService fileStorageService;
	
	@Autowired
	UserService userService;
	
	@Autowired
	SchoolRepo schoolRepo;

	@Autowired
	private DataAccess dataAccess;
	
	@Autowired
	private CityRepo cityRepo;
	
	@Override
	public String saveStudent(Student student,long schoolId, long city, MultipartFile file)  throws IOException{
		
		MyUserDetails userDetails =  (MyUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		student.setJoinDate(new Date());
		
		String fileName = fileStorageService.storeFile(file);
		String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath().path(AppConstants.DOWNLOAD_PATH)
				.path(fileName).toUriString();
		
		student.setProfileImg(fileDownloadUri);
		student.setCreator(userService.getUser(userDetails.getUsername()));
		student.setSchool(schoolRepo.findById(schoolId).get());
		student.setCity(cityRepo.getOne(city));
		
		System.out.println("saved");
		studentRepo.save(student);
		
		return "saved";
	}

	@Override
	public Student getStudent(Long id) {
		return studentRepo.findById(id).get();
		
	}

	@Override
	public List<Student> getStudents() {
		// TODO Auto-generated method stub
		return studentRepo.findAll();
	}
	
	@Override
	public List<Map<String, Object>> search(String query) {
		// TODO Auto-generated method stub
		return dataAccess.run(query);
	}

	@Override
	public List<Map<String, Object>> getStudentsBySchoolId(String query) {
		// TODO Auto-generated method stub
		return dataAccess.run(query);
	}


}
