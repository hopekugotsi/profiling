package com.developer.sp.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.developer.sp.models.Admin;
import com.developer.sp.repositories.AdminRepository;




@Service
public class UserServiceImp implements UserService{
	
	@Autowired
	BCryptPasswordEncoder encoder;
	
	@Autowired
	AdminRepository userRepo;

	@Override
	public void saveUser(Admin user) {
		user.setPassword(encoder.encode(user.getPassword()));
		user.setActive(true);
		user.setRoles("ADMIN");
		userRepo.save(user);
	}

	@Override
	public boolean isUserPresent(Admin user) {
		return false;
		// TODO Auto-generated method stub
		
	}
	@Override
	public Admin getUser(String email) {
		return userRepo.findByEmail(email).get();
	}
	/*
	@Bean
	public String getPasswordEncoder(String password) {
		return encoder.encode(password);
	}
	*/

}
