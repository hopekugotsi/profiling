package com.developer.sp.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.developer.sp.models.SchGallery;

public interface SchGRepo extends JpaRepository<SchGallery, Long> {

}
