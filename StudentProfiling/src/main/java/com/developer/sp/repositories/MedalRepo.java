package com.developer.sp.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.developer.sp.models.Medals;

public interface MedalRepo extends JpaRepository<Medals, Long> {

}
