package com.developer.sp.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.developer.sp.models.City;

public interface CityRepo extends JpaRepository<City, Long> {

}
