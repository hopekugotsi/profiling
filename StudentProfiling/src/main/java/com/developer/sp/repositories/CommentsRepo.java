package com.developer.sp.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.developer.sp.models.Comments;

public interface CommentsRepo extends JpaRepository<Comments, Long> {

}
