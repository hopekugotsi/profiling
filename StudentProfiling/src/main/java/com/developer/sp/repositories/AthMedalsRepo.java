package com.developer.sp.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.developer.sp.models.AthMedals;

public interface AthMedalsRepo extends JpaRepository<AthMedals, Long> {

}
