package com.developer.sp.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.developer.sp.models.SoccerStatistics;

public interface SoccerRepo extends JpaRepository<SoccerStatistics, Long> {

}
