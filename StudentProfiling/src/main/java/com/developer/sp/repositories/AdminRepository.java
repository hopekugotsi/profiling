package com.developer.sp.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.developer.sp.models.Admin;

public interface AdminRepository extends JpaRepository<Admin, Integer> {
	Optional<Admin> findByEmail(String email);
}
