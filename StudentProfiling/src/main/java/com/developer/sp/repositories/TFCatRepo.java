package com.developer.sp.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.developer.sp.models.TF_Cat;

public interface TFCatRepo extends JpaRepository<TF_Cat, Long> {

}
