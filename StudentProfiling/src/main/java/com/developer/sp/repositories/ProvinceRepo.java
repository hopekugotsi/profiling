package com.developer.sp.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.developer.sp.models.Province;

public interface ProvinceRepo extends JpaRepository<Province, Integer> {

}
