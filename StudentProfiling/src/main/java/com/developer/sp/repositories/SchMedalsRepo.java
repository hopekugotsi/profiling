package com.developer.sp.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.developer.sp.models.SchMedals;

public interface SchMedalsRepo extends JpaRepository<SchMedals, Long> {

}
