package com.developer.sp.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.developer.sp.models.TrackField;

public interface TFRepo extends JpaRepository<TrackField, Long> {

}
