package com.developer.sp.repositories;

import com.developer.sp.models.Event;
import org.springframework.data.jpa.repository.JpaRepository;

import com.developer.sp.models.Records;
import java.util.List;
import java.util.Map;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface RecordRepository extends JpaRepository<Records, Long> {

    @Query(value = "SELECT * FROM records WHERE isrecent = 1 AND event = ?1",  nativeQuery = true)
    Records findRecentRecord(Event event);

    @Query(value = "SELECT rc.recordid, rc.isrecent, rc.dateset, rc.worldbest, rc.nationalbest, rc.regionalbest, eve.event FROM records as rc JOIN event as eve WHERE  eve.event = ?1",  nativeQuery = true)
    List<Map<String, Object>> findRecentRecordByEventName(String event);

    @Modifying
    @Query(value = "UPDATE records  SET recent = false WHERE Recent = true AND event = ?1",  nativeQuery = true)
    int updateRecentRecord(Event event);

    
}
