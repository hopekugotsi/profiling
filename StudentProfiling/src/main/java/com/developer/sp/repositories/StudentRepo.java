package com.developer.sp.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.developer.sp.models.Student;

public interface StudentRepo extends JpaRepository<Student, Long> {

}
