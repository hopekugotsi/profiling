package com.developer.sp.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.developer.sp.models.Event;

public interface EventRepo extends JpaRepository<Event, Long> {

}
