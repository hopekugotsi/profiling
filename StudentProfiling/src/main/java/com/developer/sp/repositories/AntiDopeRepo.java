/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.developer.sp.repositories;

import com.developer.sp.models.AntiDope;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author hope
 */
public interface AntiDopeRepo extends JpaRepository<AntiDope, Long>{
    
}
