package com.developer.sp.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.developer.sp.models.Post;

@Repository
public interface PostRepo extends JpaRepository<Post, Long> {
	List<Post> findByCreatorId(int i);
}
