package com.developer.sp.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.developer.sp.models.Designation;

public interface DesignationRepo extends JpaRepository<Designation, Long> {

}
