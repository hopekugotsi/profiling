package com.developer.sp.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.developer.sp.models.School;

public interface SchoolRepo extends JpaRepository<School, Long> {
	Optional<School> findByUsername(String username);

}
