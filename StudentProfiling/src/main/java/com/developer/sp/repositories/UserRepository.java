package com.developer.sp.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.developer.sp.models.User;


@Repository
public interface UserRepository extends JpaRepository<User, Integer> {
	//Optional<User> findByUserName(String email);
	Optional<User> findByEmail(String email);
}  
