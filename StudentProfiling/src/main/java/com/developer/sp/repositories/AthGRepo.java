package com.developer.sp.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.developer.sp.models.AthGallery;

public interface AthGRepo extends JpaRepository<AthGallery, Long> {
	
	

}
